---
date: "2014-02-24"
title: "Developers: What OS Do You Use? [Wedgie/Survey]"
slug: "/2014/02/developers-what-os-do-you-use-wedgiesurvey/"
aliases: [
    "/posts/developers-what-os-do-you-use-wedgiesurvey",
    "/2014/02/24/developers-what-os-do-you-use-wedgiesurvey/"
]
category: "archive"
---

<p>Recently I had a discussion on LinkedIn where a developer said that almost everyone in the world uses Windows.</p>

<p>While that may be true, I don't agree that it means as developers we should also use Windows machines as our primary machines. What OS do you develop on?</p>

<p><script src='https://www.wedgies.com/js/widgets.js'></script><noscript><a href='https://www.wedgies.com/question/530c05c1aedea90200000005'>Vote on our poll for What OS do you develop on?!</a></noscript></p>

<div class='wedgie-widget' wd-pending wd-type='embed' wd-version='v1' id='530c05c1aedea90200000005' ></div>


