---
date: "2014-01-17"
title: "AlfredApp Theme: Flatland"
slug: "/2014/01/alfredapp-theme-flatland/"
aliases: [
    "/posts/alfredapp-theme-flatland"
]
---

<p class="intro group">
  <a href="https://www.alfredapp.com?referrer=realchaseadams" data-gae="post*image*alfred-app"><img src="https://www.realchaseadams.com/imgs/2014/01/alfredapplogo.png" alt="alfredapplogo" width="150" height="124" class="alignleft unstyled size-full wp-image-300" /></a> <a href="https://www.alfredapp.com?referrer=realchaseadams" data-gae="post*text*alfred-app">AlfredApp is one of the best productivity hack apps</a> you can install on your Mac. I use it multiple times a day every day, so it stands to reason that it is my favorite, so I decided to theme it.
</p>

<p>My friend <a href="https://natewienert.com/">Nate Wienert</a> shared the <a href="https://github.com/natew/flatland">Flatland theme for SublimeText 2</a> (which is an awesome theme), and it quickly became my favorite color scheme for everything, SublimeText, <a href="https://github.com/realchaseadams/chrome-devtools-flatland-UI">Chrome Canary Web Inspector</a> and now AlfredApp.</p>

<p><a href="alfred://theme/searchForegroundColor=rgba(205,212,213,1.00)&amp;resultSubtextFontSize=0&amp;searchSelectionForegroundColor=rgba(255,255,255,1.00)&amp;separatorColor=rgba(51,51,51,1.00)&amp;resultSelectedBackgroundColor=rgba(39,100,147,1.00)&amp;shortcutColor=rgba(56,139,246,1.00)&amp;scrollbarColor=rgba(51,51,51,1.00)&amp;imageStyle=7&amp;resultSubtextFont=Monaco&amp;background=rgba(29,31,33,1.00)&amp;shortcutFontSize=2&amp;searchFontSize=4&amp;resultSubtextColor=rgba(115,115,115,1.00)&amp;searchBackgroundColor=rgba(46,50,53,1.00)&amp;name=Flatland&amp;resultTextFontSize=2&amp;resultSelectedSubtextColor=rgba(204,213,210,1.00)&amp;shortcutSelectedColor=rgba(197,212,212,1.00)&amp;widthSize=3&amp;border=rgba(48,52,55,0.77)&amp;resultTextFont=Helvetica&amp;resultTextColor=rgba(151,155,147,1.00)&amp;cornerRoundness=3&amp;searchFont=Lucida%20Grande&amp;searchPaddingSize=3&amp;credits=Chase%20Adams&amp;searchSelectionBackgroundColor=rgba(56,139,246,1.00)&amp;resultSelectedTextColor=rgba(253,254,254,1.00)&amp;resultPaddingSize=3&amp;shortcutFont=Geneva"><img src="https://www.realchaseadams.com/imgs/2014/01/Screen-Shot-2014-01-18-at-6.27.00-AM.png" alt="Flatland AlfredApp Theme" width="648" height="303" class="alignnone size-full wp-image-305" /></a></p>

<p>If you're using AlfredApp, you can install my custom <a href="alfred://theme/searchForegroundColor=rgba(205,212,213,1.00)&amp;resultSubtextFontSize=0&amp;searchSelectionForegroundColor=rgba(255,255,255,1.00)&amp;separatorColor=rgba(51,51,51,1.00)&amp;resultSelectedBackgroundColor=rgba(39,100,147,1.00)&amp;shortcutColor=rgba(56,139,246,1.00)&amp;scrollbarColor=rgba(51,51,51,1.00)&amp;imageStyle=7&amp;resultSubtextFont=Monaco&amp;background=rgba(29,31,33,1.00)&amp;shortcutFontSize=2&amp;searchFontSize=4&amp;resultSubtextColor=rgba(115,115,115,1.00)&amp;searchBackgroundColor=rgba(46,50,53,1.00)&amp;name=Flatland&amp;resultTextFontSize=2&amp;resultSelectedSubtextColor=rgba(204,213,210,1.00)&amp;shortcutSelectedColor=rgba(197,212,212,1.00)&amp;widthSize=3&amp;border=rgba(48,52,55,0.77)&amp;resultTextFont=Helvetica&amp;resultTextColor=rgba(151,155,147,1.00)&amp;cornerRoundness=3&amp;searchFont=Lucida%20Grande&amp;searchPaddingSize=3&amp;credits=Chase%20Adams&amp;searchSelectionBackgroundColor=rgba(56,139,246,1.00)&amp;resultSelectedTextColor=rgba(253,254,254,1.00)&amp;resultPaddingSize=3&amp;shortcutFont=Geneva">Flatland Theme</a>.</p>

<p>If you find this useful, be sure to share it on <a href="https://twitter.com/share?url=https://www.realchaseadams.com/2014/01/18/alfredapp-theme-flatland/" class="trigger-share twitter">twitter</a>!</p>
