---
date: "2014-01-28"
title: "Sometimes I have to just say No"
slug: "/2014/01/why-i-say-no/"
aliases: [
    "/posts/sometimes-i-have-to-just-say-no/",
    "/2014/01/sometimes-i-have-to-just-say-no/",
    "/2014/01/29/why-i-say-no/"
]
category: "archive"
---

I am incredibly honored, humbled and thankful when people ask me to help them with something they're working on. It's a great joy knowing my insights are valued.

**But I am at capacity.**

Like anyone else, I have a lot of things going on. Between work, home and my personal time, I have to manage my time to be effective. That's not to say I don't have extra time, but I have to be intentional about how I spend it. This means having to say "Thank you, but I can't help with that right now".

I prioritize time with God, with Jackie and building my legacy are my priorities. If something doesn't fall into one of those categories, I have to really consider whether it's something I should do.

I wrote a blog post outlining my [cannonballs — the themes that allow me to cast vision, determine what's important and get things done](https://bit.ly/19RXcgu) — explaining why I take on certain things and not others.

I don't want to give anyone, especially family or friends, less than 100% of what I have to give. If I don't think I can give you that, I'll most likely say "no". I hope you understand it isn't anything personal or impersonal, it's just me trying to create margin in my life.

This is the path I decided to take. It is what it is. I say no to things. **But my decision to say no shouldn't deter you from trying to find an alternative route to your question.**

## Why You Think I Have The Answers You Seek

Most questions I get are programming questions. I started learning about programming by using Google and books. I stumbled through things, and consequently I'm a big believer in bootstrapping yourself. So here are some ways I approach a question I have before asking someone I know might have an answer:

### Google

This may sound tongue-in-cheek, but it's not. Google is a great resource for finding answers to your questions. There are a lot of smart people who've had the same problems and documented them. These are great resources and easily found by using a _well formed_ Google search.

### Twitter

Twitter is amuck with information and starting points. Ask your question there, use a hashtag or two about the topic and see what happens. Use Twitter's search functionality to find keywords about the subject you're interested in. It's one of the strongest communities I know of, so you should leverage that.

### WordPress Codex, WordPress Forum or Mozilla Developer Network

A lot of people come to me with questions about "broken" WordPress, Javascript or HTML problems. 99.9% of the time nothing is broken, it's just a typo or a misunderstanding of how something works. These three sites have great resources for people who've dealt with the exact same things. I keep them as Alfred App web searches because they're often the first places I'll go when people have questions:


- [WordPress Codex](https://codex.wordpress.org/)
- [WordPress Support Forum](https://wordpress.org/support/)
- [Mozilla Developer Network](https://developer.mozilla.org/en-US/)


I'm not any smarter than anyone else, I've just learned how to ask better questions because I've broken my share of things.

If there are far better resources for what you need, who do exactly what you're asking and will give you better results, I'll point you along to them. I'm good at that and it doesn't take a lot of time.

If you want me to help with something, ask. I can't guarantee a "yes". I also won't guarantee a "no". I can guarantee, however, that I'll look at how it fits into my life before giving you either answer. I want to help. I don't want anyone to feel ignored. But I have to do what's right for me, my family and my time first.
