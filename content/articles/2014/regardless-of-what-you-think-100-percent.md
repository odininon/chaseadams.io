---
date: "2014-02-05"
title: "Regardless of what you think, I only have 100% to give"
slug: "/2014/02/regardless-of-what-you-think-i-only-have-100-to-give/"
aliases: [
    "/posts/regardless-of-what-you-think-i-only-have-100-to-give"
]
category: "archive"
---

<p class="intro">
  Have you ever thought about how much of yourself you have to give? I often hear "I'm 110 percent invested in thing x". This isn't possible. We all only have 100 percent to give.
</p>

<p>I totally understand the idea of saying "I'm 110 percent invested" is to assure others I'm totally invested, but there are two critical failures here:</p>

<ol>
<li><p><strong>We can only offer 100 percent of ourselves.</strong> I can't muster an extra 10 percent. If I could, that would become my new 100 percent.</p></li>
<li><p><strong>I'm negatively invested everywhere else.</strong> If I <em>could</em> give 110% to just one thing, inevitably any other task I attempt will be negatively impacted by at least 10 percent.</p></li>
</ol>

<p><strong>I only have 100 percent to give. And that's if I'm fully efficient.</strong></p>

<p>Why do we set these unrealistic expectations for ourselves? What makes <em>me</em> think that I am the exception, that I have any more than 100 percent of myself to give?</p>

<blockquote>
  <p>We can't perform two tasks at 100 percent efficiency. I can hardly do one thing at 100 percent efficiency.</p>
</blockquote>

<h2>Multi-Tasking: The Silent Killer of Productivity</h2>

<p>This fatal flaw, <em>that we can give more than 100 percent</em>, rears it's head anytime I hear someone talking about <em>multi-tasking</em>.</p>

<p>I read a productivity hack on LinkedIn from <a href="https://www.linkedin.com/today/post/article/20140121123309-44558-productivity-hacks-the-powerpoint-workout?trk=vsrp_influencer_content_res_name&amp;trkInfo=VSRPsearchId%3A455612621390480238276%2CVSRPtargetId%3A5826659433060909056%2CVSRPcmpt%3Aprimary">an entrepreneur who works on her power points while she works out</a>. I love maximizing time as much as anyone else, <strong>but multi-tasking is a myth</strong>. We can't do two things at 100 percent efficiency. I can hardly do one thing at 100 percent efficiency.</p>

<p>If I'm doing two tasks at once, they're each going to be at a reduced capacity. What's worse is I'm really performing three tasks: The first two tasks and a third task of balancing out the first two.</p>

<h2>Kill the Multi-Tasking Myth</h2>

<p>If you value your time as much as I do, it's hard to give up multi-tasking. <strong>But I believe in you,</strong> that you can give up multi-tasking and stay productive. To help you get started, here are some things I've found boosted my productivity.</p>

<h3>Say No. A lot.</h3>

<p>I created a list of <a href="https://www.realchaseadams.com/2014/01/21/cannonballs-the-key-to-casting-vision/">11 cannonballs, or themes, for my life</a>, and this list has been the guide to how I decide whether to say yes or no. If it's not a cannonball, I say no.</p>

<p>I've found <a href="https://www.realchaseadams.com/2014/01/29/why-i-say-no/">saying no to certain requests</a> means I have "less" to do, which means I have more time to focus on the things I've decided are important for me right now. Because I have more time, multi-tasking is less of an issue.</p>

<h3>Pop in Headphones</h3>

<p>I realize not everyone works in an environment where headphones are acceptable, but if you do, put them in. Create playlists that you know will help you focus, invest in a good pair of headphones, and go beast mode on one task.</p>

<p>I listen to music in the mornings while I work at home and it's helps me stay focused and on task.</p>

<p>To help get you started, some of the "sounds" I use are:</p>

<ul>
<li><a href="https://coffitivity.com/">Coffitivity: Coffee Shop white noise to boost productivity</a> </li>
<li><a href="https://open.spotify.com/album/1yvS0zkkIUj08RdjX3wdNd">Vitamin String Quartet Performs Modern Rock Songs</a> on Spotify</li>
<li><a href="https://open.spotify.com/user/realchaseadams/playlist/5QHiYfUvibrxB1o9LfykPT">Writing Playlist</a> on Spotify</li>
<li><a href="https://itunes.apple.com/us/album/into-the-trees/id378355722">Zoe Keating's Into the Woods</a> on iTunes</li>
</ul>

<h3>Create A Manageable Todo List</h3>

<p>If you're todo list for today has more than 4 attainable tasks, you need to prune. If you're not using a todo list, try out Wunderlist, any.do or if you use github, <a href="https://lifehacker.com/why-a-github-gist-is-my-favorite-to-do-list-1493063613">a github todo list</a>.</p>

<p>If you have an item that hangs out on your todo list for longer than a day, break it down into multiple attainable tasks and only put one to four of those on your todos for today.</p>

<p>Keep doing this until you're eventually creating "done in a day" tasks every time. This approach will take up more time in the beginning, but it'll make you better at estimating the time tasks take and give you a better sense of getting things done.</p>

<h3>Use a timer and work in short bursts</h3>

<p>I have an app called Healthier that I use for splitting up my working minutes and resting minutes. It forces me to stop and take a break, but it also enables me to set a period of time to focus on one thing. It's loosely based on the <a href="https://pomodorotechnique.com/">Pomodoro Technique</a>, which I recommend checking out.</p>

<h3>Multi-tasking is not a baby and the bathwater issue</h3>

<p>There is some effective multi-tasking: listening to Podcasts while walking my dog or while filling our water containers is generally productive. Anytime I can perform a task that requires little mental overhead alongside a passive task, I'll do it, but I consider this situation a one-off.</p>

<p><strong>Stop trying to multi-task the important things.</strong> Give 100 percent of yourself to one thing at a time. If you're trying to multi-task you'll find that you're actually wasting time, will power and defeating the objective of multi-tasking.</p>

<p class="marker">
  So what do you think: Can you multi-task and still give each task 100 percent or is it a myth that multi-tasking truly saves you time?
</p>
