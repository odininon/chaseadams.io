---
date: "2014-02-25"
title: "Huffington Post Article: You May Never Become A Developer"
slug: "/2014/02/huffington-post-article-you-may-never-become-a-developer/"
aliases: [
    "/posts/huffington-post-article-you-may-never-become-a-developer/"
]
description: "I pursued learning programming and software development with every fiber of my being, trying to learn anything I could about programming, solving problems and shipping products."
category: "archive"
---

f you asked me 10 years ago: “Chase, do you think you might be a web developer in 2014?” I would most likely have laughed and told you that I don’t have the patience to learn what I would need to know to legitimately build the web.

You see, at the time I was on track to graduate in two years with a degree in Environmental Science and a minor in Education, with the hopes of teaching High School Ecology and sharing the wonders of the natural world.

Two years later, in 2008, with a month left before graduation and opportunities lined up, my dad called and asked me to come home to help with our family lumber yard. The industry was failing and people weren’t building houses, so the main source of revenue was essentially non-existent.

To make a long story short, I rode shotgun on deliveries and learned to write code. Learning new technologies wasn’t something I was passionate about at first, but it became an insatiable thirst.

I rebuilt our family businesses website during whatever free time I had and increased our web traffic from 10 visits a day to around 500 visits a day.

I pursued learning programming and software development with every fiber of my being, trying to learn anything I could about programming, solving problems and shipping products.

Eventually, through a series of good choices and great opportunities our family business rethought our business model and became one of the few lumber yards left standing in Atlanta. I continued to learn programming on my own and after spending countless hours in my spare time learning I made it on to the website team at Zappos.com, a major Enterprise-level online retailer.

I never thought I’d be a programmer, a web developer, a software engineer or an advocate of others learning the concepts that make a software engineer, but here I sit today, solving problems inside and outside of Zappos with code.

I had no idea that the choices I made and the hard work I had put in would bring me to where I am, but I made it, **I’m still chasing it and I love it.**

If you want to learn to become a programmer, I encourage you to start with hour of code. There are great resources there that explain and walk you through what it really means to be a programmer: **solving problems.**

# Choose to Choose

You may never become a programmer, but I hope you choose to give it a shot. If you decide at some point it’s not for you, choose to try something else. Repeat the cycle until you find your passion. Choose to pursue it. Make time for it.

If you find your passion and you pursue it, I promise you won’t be disappointed. You may not be successful in the normal sense, but if you pursue it, continue to rethink the conventional ways you can use it, you’ll find satisfaction in what you’re doing.

Today I hope you choose to choose. Know who you want to be and choose to pursue it with every fiber of your being. Your circumstances may play a role in where you are, but only insofar as how they impacted your decision to choose.