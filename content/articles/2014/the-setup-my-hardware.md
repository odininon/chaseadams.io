---
date: "2014-01-26"
title: "The Setup: My Hardware & Software for Work & Play"
slug: "/2014/01/the-setup-my-hardware-software-for-work-play/"
aliases: [
    "/posts/the-setup-my-hardware-software-for-work-play"
]
---

<p class="intro">
  Thanks to my hosting provider, <a href="https://www.digitalocean.com">Digital Ocean</a>, I stumbled over this really cool site called <a href="https://www.usesthis.com">The Setup</a> (<a href="https://www.twitter.com/usesthis">@usesthis</a> on twitter). The Setup is a <em>"collection of nerdy interviews asking people from all walks of life what they use to get the job done."</em> So I thought I'd share the things I use with the hope that they'll help you.
</p>

<h2>Who are you, and what do you do?</h2>

<p>I'm Chase Adams and I'm a mobile web/front-end developer at Zappos.com. I'm passionate about creating products that subtly wow and helping people find their passion. I also teach &amp; mentor other Zappos employees on their journey into becoming a developer. I love working with web technologies like HTML &amp; CSS, Javascript (front-end and back-end) and Ruby. I tweet at <a href="https://www.twitter.com/realchaseadams/">@realchaseadams</a>.</p>

<h2>What hardware do you use?</h2>

<p>At work I have a <a href="https://www.apple.com/macbook-pro/">15" MacBook Pro</a> that has been mostly for doing front-end development for Java applications. It's a workhorse, but now I'm doing a lot more Node development, so it's analogous to trying to kill a mosquito with a tank. I use a Mighty Mouse and a <a href="https://www.amazon.com/Logitech-Wireless-Solar-Keyboard-K750/dp/B005L38VRU/ref=sr_1_2?s=electronics&amp;ie=UTF8&amp;qid=1390399349&amp;sr=1-2&amp;keywords=Logitech+Wireless+Solar+Keyboard+K750">silver Logitech Wireless Solar Keyboard K750 for Mac</a>. The keyboard is pretty sweet, doesn't require batteries and is consistent with the Apple 'brushed aluminum' style.</p>

<p>At home I have a <a href="https://www.apple.com/macbook-air/">13" MacBook Air</a>. It's lean, it's fast and most importantly it gets the job done. The battery lasts for hours, which makes it nice to detach from the wall for long periods of time. I do a lot of light weight application development in Ruby &amp; NodeJS, so the specs on it are perfect for local development.</p>

<p>I'm an audiophile, so I believe everyone should own a nice pair of headphones. The <a href="https://www.amazon.com/Bose-326223-0080-Bose%C2%AE-Mobile-Headset/dp/B0043WCH66">Bose Mie2 in-ear headphones</a> have been the best pair I've found so far: they never fall out, provide great range and are really comfortable.</p>

<p>I recently switched from an iPhone 3GS to a <a href="https://www.htc.com/www/smartphones/htc-one/">HTC One</a>. I like the HTC One, but I miss a few of the baked in features of the iPhone (geofencing todos in reminders, quick access to non-security risk features when Exchange is setup on the phone, buttons spanning the full width of their parent elements (I know, it's picky)).</p>

<p>I'm also a big <a href="https://www.realchaseadams.com/2014/01/05/my-15-essential-reads-for-professional-growth-in-2014/">fan of reading</a>, so I have an old school <a href="https://www.amazon.com/gp/product/B007HCCNJU/ref=amb_link_367867082_6?pf_rd_m=ATVPDKIKX0DER&amp;pf_rd_s=left-1&amp;pf_rd_r=0C0HCD5J1ZJBTK29N821&amp;pf_rd_t=101&amp;pf_rd_p=1624898862&amp;pf_rd_i=133141011">Kindle</a>.</p>

<p><img src="https://www.realchaseadams.com/imgs/2014/01/2014-01-26-09.07.02-edited.jpg" alt="2014-01-26 09.07.02-edited" width="250" height="250" class="alignleft size-full wp-image-497" /> I'm really into bags, so I'd be remiss to not mention my <a href="https://www.zappos.com/patagonia-black-hole-pack-black">Patagonia Black Hole Pack</a>. I keep toiletries, two computers, all my MacGyver gear, Kindle and chargers in it and it's held up really well.</p>

<p class="group">
  I'm a huge fan of physically writing things down, so I always have a <a href="https://amzn.to/1eWBFCr">Rhodia DotPad 3.25 in x 4.75 in dot matrix notepad</a> and <a href="https://amzn.to/1jT1iG2">Pentel EnerGel NV Liquid Gel Pen, 0.5mm, Needle Tip, Black Ink in my pocket</a>. I love the idea of grids, but hate lines, so a dot grid works perfect for me. I also like that the top folds over really nicely when you're writing, all the pages are perforated and it fits cleanly in my front pocket.
</p>

<h2>And what software?</h2>

<p>I'm really into productivity, so almost everything I have installed is to help leverage my time.</p>

<p>I use <a href="https://www.sublimetext.com/">SublimeText 2</a> for all of my text editing, software development and taking notes. The keyboard shortcuts baked in save me tons of time, the command palette helps find files faster and the community support via plugins is really active.</p>

<p>For my workflow I use <a href="https://www.alfredapp.com/">Alfred 2</a>. Alfred is a quick launching search palette that will allow you to quickly access applications, contacts, calculations and custom web queries. I paid for the <a href="https://www.alfredapp.com/powerpack/">PowerPack</a> which allows you to create hotkeys to workflows and applications, so I have all of the apps I use on a daily basis mapped to CMD + # (ie to open SublimeText 2 I just type CMD + 4).</p>

<p>I use <a href="https://getpocket.com/">Pocket</a> to save posts and articles that I want to read later, <a href="https://evernote.com/">Evernote</a> to clip thoughts and ideas &amp; <a href="https://www.wunderlist.com/en/">Wunderlist</a> for daily reminders and todos.</p>

<p>I spend most of my day in the browser developing for the web, and <a href="https://www.google.com/intl/en/chrome/browser/canary.html">Chrome (Canary Nightly)</a> is my browser of choice. I find DevTools are really well done and the DevTools team is constantly working really hard at making it a developer powerhouse. Some extensions I find really useful are <a href="https://chrome.google.com/webstore/detail/colorzilla/bhlhnicpbhignbdhedgjhgdocnmhomnp?hl=en">ColorZilla (an in-page color picker)</a>, <a href="https://chrome.google.com/webstore/detail/json-prettifier/kccpfgilgmgbipamhohknpokhibinhhj">JSON Prettifier</a> and <a href="https://chrome.google.com/webstore/detail/postman-rest-client/fdmmgilgnpjigdojojpjoooidkmcomcm?hl=en">Postman (REST client)</a>. I have a custom <a href="https://chrome.google.com/webstore/detail/devtools-theme-flatland/ghngaepikegoilihhbhdipfbfifhkeeo">theme for my DevTools that's a port of my SublimeText theming</a>.</p>

<p>I host all of my personal websites on <a href="https://www.digitalocean.com">Digital Ocean</a>. They have great documentation on pretty much anything you'd want to setup, and the sky is the limit. The ability to have Root access is huge.</p>

<h2>What would be your dream setup?</h2>

<p>The setup I have at work is pretty sweet, so it'd be awesome to have it home. It'd be nice to have an external monitor like the <a href="https://store.apple.com/us/product/MC914LL/B/apple-thunderbolt-display-27-inch">Apple Thunderbolt 27" Display</a> and a <a href="https://www.thehumansolution.com/uplift-900-electric-sit-stand-desk-black.html?utm_source=Google%2BShopping&amp;utm_medium=cpc&amp;utm_medium=cpc&amp;utm_campaign=Google%2BShopping&amp;gdftrk=gdfV2929_a_7c132_a_7c5187_a_7cUPL901&amp;gclid=CPX-pYH7kbwCFcQ9Qgod63AABg">UpLift 900 Electric Sit-Stand Desk (Black)</a>.</p>


