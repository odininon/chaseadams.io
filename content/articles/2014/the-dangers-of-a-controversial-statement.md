---
date: "2014-01-14"
title: "The Dangers of a Controversial Statement Without Context"
slug: "/2014/01/the-dangers-and-power-of-a-statement-without-context/"
aliases: [
    "/posts/the-dangers-of-a-controversial-statement-without-context"
]
category: "archive"
---

"I’m Dating Someone Even Though I’m Married."*

This was the title for a blog post I have seen in my news feed lately. No context. No immediate qualifiers to give it any form. Just good, old-fashioned link bait**. I was under no illusion that it was probably some guy talking about dating his wife, so I didn't read the article. I mean really, how many posts do you see in your news feed and click on?

Then I saw it pop up again 4 or 5 times in my Twitter and Facebook feeds, so I read it. It was exactly what I thought it was and without the spectacular title I don't think I would've had any thoughts on it outside of the ones I already have as a husband. Don't get me wrong, I am a total advocate for getting to know your spouse better, but this isn't a <a href="https://www.youtube.com/watch?v=9CS7j5I6aOc">mind explosion moment</a>.

My biggest concern is this: <strong>What about the people who skim their feeds and don't know anything about the art of writing catchy titles?</strong> It took me multiple instances of seeing it to finally look at it, and I've done a lot of research on driving blog traffic through good titles, so I had an idea it was about dating your spouse.

By making a blanket statement like, "I'm dating someone even though I'm married" without any qualifiers or context, an author essentially releases someone reading that from the responsibility of reading the article and enables them to say, "welp, I guess if this guys says it's okay and other people are alright with it, I can go for it."

I think there's a place for titles that drive traffic to your site. I can appreciate a good rant. What I don't agree with, and find terribly dangerous, is valuing marriage and leveraging the innuendo of infidelity to get people to click through to your post about how you value marriage. The context should be the power that drive people to read your thoughts, not the title.

<small><span>*</span> This is the title of a post from Jarrid Wilson. I don't want to give this link any juice, so if you're interested in reading it, you can copy/paste it or google it yourself, but I know I need to at least give the author attribution: https://jarridwilson.com/im-dating-someone-even-though-im-married/</small>

<small markdown="1"><span>**</span> Link bait: "...also known as clickbait, is any content or feature, within a website, designed specifically to gain attention or encourage others to link to the website." ~ <a href="https://en.wikipedia.org/wiki/Link_bait">Wikipedia</a></small>


