---
date: "2014-02-27"
title: "I Don't Write So Good: A Call To Write Better"
slug: "/2014/02/i-dont-write-so-good-a-call-to-write-better/"
aliases: [
    "/posts/i-dont-write-so-good-a-call-to-write-better",
    "/2014/02/28/i-dont-write-so-good-a-call-to-write-better/"
]
category: "archive"
---

I am not a writer by nature.

Poll any of my professors, specifically those in the English departments, and I believe the general sentiment would be that I scraped by on my imagination and unquantifiable amounts of editing.

Decades later and that hasn't changed. I still have a fear of putting keystroke to editor, afraid that what I write won't be compelling or coherent to even the most intelligent readers. When I do, I _always_ write sentences that need at least two revisions. In fact, that sentence had three.

The problem is that having thoughts and ideas is a very different creature to expressing those thoughts and ideas through written word. My fear isn't that I have good ideas, it's that I just can't express them with the full power and merit that they deserve.

A friend said recently: "not everyone is a writer". True as it may be, we most likely disagree on the why: __Most people just choose not to.__

## A Call To The Writer Revolution

I always ask people who have arrived where I one day want to be: "How Did You Get There?" I'm certainly not the writer in frequency, quality or consistency that I'd like to be, but I'm to the point that enough people ask me that question, so here are some of the things I've done to get where I am today.

### Write for 10 minutes a day

Start right now. I write something for at least 10 minutes a day. I find a time to be intentional about those 10 minutes and I just write. Think of it as fuel-injection for your brain. Your body wants to maintain homeostasis (a la watch tv) and your brain is no different. However, if your brain (or body for that matter) receives stimulus, it's like jump starting a car. It's ready to go.

### Keep A Large Pool Of Ideas

I have an Evernote notebook that is a blank note with a title of something I'd like to write about. There are over 150 notes in that notebook at a time. Having a direction is 90% of the battle. Most of these notes never have yet to see the light of day, but having a pool gives me something to pick from.

### Read On Writing Well

Pick up a copy of "On Writing Well: The Classic Guide to Writing Nonfiction" by William Zinnser. Read a chapter a day. Take that chapter and superimpose it on your personal mission to write.

### Read, Read, Read, Read Some More, Then Write A Little

It might seem counter-intuitive to read 4 times as much as I write, but reading is a huge part of my process for two reasons:

- __It helps me to define my writing style.__ I read content about different topics, written by different people and cherry pick the styles that I like.
- __It stokes my creative fire.__ When I read, 9 times out of 10, my mind goes on a creative rabbit trail. Reading about other people's ideas and thoughts either help me further solidify my own or give me brand new thoughts to pursue on my own.

Use tools like Guy Kawasaki's [Alltop.com](https://alltop.com) to find content written about the things you're interested in. Write down highlights that resonate with you, as well as the thoughts that they inspire, writing these summaries and memorable quotes are great springboards for when you get stuck on what to write in the future as topics to write about.

## Join Me

My _desire_ is to write such powerful, compelling messages that the first thing you want to do when you wake up in the morning is check to see if I've posted something new. Many of my peers, co-workers and friends have said, "I want to start blogging". If you're one of those people, make this your goal, try some of the things I'm doing and let me know how it's going.

__I want you to choose to write.__ What should you write about? It doesn't matter, but start with what you know. Once you start, the world is your oyster.

What about you? What tips do you have for learning to be a better writer? Share in the comments below!
