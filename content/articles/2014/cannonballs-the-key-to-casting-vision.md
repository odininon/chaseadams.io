---
date: "2014-01-21"
title: "Cannonballs: The Key to Casting Vision, Determining What's Important & Getting Things Done"
slug: "/2014/01/cannonballs-the-key-to-casting-vision/"
aliases: [
    "/posts/cannonballs-the-key-to-casting-vision-determining-whats-important-getting-things-done"
]
category: "archive"
---

<p class="intro">
  Floater. Endearing term, isn’t it? Unfortunately, that’s the best term I can come up with for how I’ve approached life. Letting the slow drift of a lazy river carry me in a meandering fashion, subject to the whims of the current.
</p>

<p>If you’ve ever been to the south, you know exactly what this looks like: A Honey Boo Boo-esque child, lazing in an inner tube on a lazy river at a three naut drift, red as a tomato from insistence of not wearing sunscreen sipping her special Go Go juice, a cocktail of Mountain Dew and Red Bull, and munching on a family-sized bag of M&amp;Ms...That’s how I feel when I look back on the past 15 years of life, the years when the power to choose was truly in my hands, the years that I chose to just float.</p>

<p>I don’t know what shook me into action. Maybe it’s the disturbing thought that I, in some way, parallel Honey Boo Boo. It could be the fact that I’m hitting 30 in two months and didn’t hit the 30 million under 30 list. Maybe I just woke up on my own. Whatever it was, it moved me to create cannonballs.</p>

<p>I first heard the term Cannonballs used at a Zappos all-hands. Cannonballs are big ideas that allow you to create a framework to decide which ‘bullets’ to use. They’re a way to more easily automate the decisions you make on a day-to-day basis by asking: “Does what I’m about to do support cannonball X?”</p>

<h2>How I discovered my cannonballs</h2>

<p>I started with what I wanted to achieve this year — I asked, “What will make me look back on 2014 and feel proud?” This is a hard question to answer if you don’t have a personal mission statement. It’s on par with trying to build a house on sand with no foundation. Without a mission statement, there is nothing to reign in your vision, to give it structure.</p>

<p>I wrote mine in the form of <a href="/2013/10/my-eulogy/">My Own Eulogy</a>, an exercise suggested by Steven Covey in the Seven Habits of Highly Effective people:</p>

<blockquote>
  <p>“The world has lost a great believer, a great husband, a great leader, a great learner, a great teacher, a passionate engineer.” <a href="https://www.realchaseadams.com/2013/10/15/my-eulogy/">(Excerpt from "My Eulogy")</a></p>
</blockquote>

<p>Knowing what I want the end to look like prepares me for creating themes for the year. I took these six life roles and created <strong>11 cannonballs</strong>:</p>

<ul>
<li><p><strong>Pursuit, Intent, Engagement</strong></p>

<ul>
<li>Intentionality is taking a map, charting your course, going on the adventure and being attentive. It’s the opposite of a lazy river. Don’t be passive about relationships. Every relationship is strengthened by action and weakened by inaction. Most importantly: God. Second: Jackie. Third: Everyone else. </li>
</ul></li>
<li><p><strong>Move More.</strong></p>

<ul>
<li>Be at the border of being conscious and being obsessed, but always err on the side of conscious. An obsessed life is a life truly unlived.</li>
</ul></li>
<li><p><strong>Architect Better Habits.</strong></p>

<ul>
<li>Habits enforce the automation of reactions. Better habits lead to better reactions, freeing up my mind for more important things.</li>
</ul></li>
<li><p><strong>Automate. Automate. Automate.</strong></p>

<ul>
<li>Manually doing <em>everything</em> leads to automatically losing time. Time is important, many things can be automated: automate those things.</li>
</ul></li>
<li><p><strong>Focus on Sticky Ideas. (Anatomy &amp; Execution)</strong></p>

<ul>
<li>I have ideas. Find the ones that will stick and put resources into those.</li>
</ul></li>
<li><p><strong>Learn. Create. Educate. Repeat.</strong></p>

<ul>
<li>The scalable cycle: Learn something, build something with it, share it with someone else, encourage them to do the same. Create LCE Feedback loops.</li>
</ul></li>
<li><p><strong>See the Good.</strong></p>

<ul>
<li>It’s really easy for me to quickly come up with, “My only concern…” statements. Stop expressing concern, simply know it’s there. Instead of expressing the <em>concern</em> focus on the <em>potential</em>.</li>
</ul></li>
<li><p><strong>Build A Platform.</strong></p>

<ul>
<li>I’m not perfect, or epic or even moderately awesome, but I do have things to offer. Share those things. Become a Maven. Build a community.</li>
</ul></li>
<li><p><strong>Unfog the Mirrors. ie Communicate More Clearly</strong></p>

<ul>
<li>Foggy mirrors lead to a misinterpretation of what’s being reflected. Learning to express myself more clearly will make for less foggy mirrors.</li>
</ul></li>
<li><p><strong>Choices.</strong></p>

<ul>
<li>Every path your life takes at one of the infinite forks of your journey starts with a choice, so learn to make them better by knowing how they work.</li>
</ul></li>
<li><p><strong>Rest.</strong></p>

<ul>
<li>Don’t kill yourself. Take time to rest. Not sleep, rest.</li>
</ul></li>
</ul>

<p>That’s it: 11 Cannonballs for 365 days. Every moment I need to make a decision, I now have a framework to immediately know if that decision is a go or a no go. How I spend my time, what books I read, what websites I visit, who I hang out with, what goals I set: They all stem from these cannonballs.</p>

<h2>A Few Cautions</h2>

<p><strong>Don’t confuse cannonballs with core values.</strong> Core Values are a fixed point of reference. They’re the lens that you evaluate the world through, which ultimately help you create your cannonballs. Cannonballs are fluid, they can change, but they change based on your fixed point of reference.</p>

<p><strong>Don’t have too many cannonballs.</strong> I believe 10 or less are good for an individual. More than 10 cannonballs can lead to unhealthy expectations as well as a fragmented life. I broke my own rule, but I think having rest as the eleventh cannonball is a good exception.</p>

<p><strong>You won’t always use your cannonballs.</strong> I don’t expect to always pursue life with a cannonball as a catalyst, it’s too rigid. I built rest in as a cannonball after looking back through the list to give myself an “out” when the other cannonballs are too demanding, without feeling like I’m failing.</p>

<p class="marker">
<em>What strategies do you use to help you better define what your year should look like? Do you have any cannonballs? Comment below or let’s start a conversation on <a href="#" class="trigger-share twitter">twitter</a>!</em>
</p>


