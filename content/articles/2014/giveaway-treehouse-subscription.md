---
date: "2014-01-16"
title: "Giveaway: A Chance to Win A 3 Month Membership to Treehouse — A Technology Learning Site"
slug: "/2014/01/giveaway-a-chance-to-win-a-3-month-membership-to-treehouse-a-technology-learning-site/"
aliases: [
    "/posts/giveaway-a-chance-to-win-a-3-month-membership-to-treehouse-a-technology-learning-site",
    "/2014/01/16/giveaway-a-chance-to-win-a-3-month-membership-to-treehouse-a-technology-learning-site/"
]
category: "archive"
---

<p class="intro callout">
  I'm teaming up with Team Treehouse to give away five 3 month memberships to their website — a better way to learn technology.
</p>

<p>A month ago I wrote a post about why <a href="https://www.realchaseadams.com/2013/12/09/my-story-why-i-believe-in-the-hour-of-code-initiative/">I believe everyone should learn to code</a>. The feedback was tremendous and the most popular question people asked was: <strong>"Where do I start?"</strong></p>

<p>I'm going to share 3 resources over the course of the next three months where you can go to get started with your journey into becoming a programmer. The first is <a href="https://www.teamtreehouse.com">Treehouse</a>.</p>

<p><a href="https://www.teamtreehouse.com"><img src="https://www.realchaseadams.com/imgs/2014/01/Tree-House.png" alt="Tree-House" width="150" height="141" class="alignleft size-full wp-image-264" /></a> <strong>Teamtreehouse.com is a great place to start your journey of learning to code.</strong> Co-founded by Ryan Carson, Treehouse believes in "bringing affordable Technology education to people everywhere, in order to help them achieve their dreams and change the world."</p>

<p class="group">
  I had the opportunity to use Treehouse last year to sharpen my skills in Ruby and terminal and I found it both highly addicting as well as incredibly informative.
</p>

<blockquote>
  <p>Treehouse currently has a <a href="https://teamtreehouse.com/subscribe/plans?trial=yes&amp;referrer=realchaseadamsdotcom">free 14 day trial</a> so there's no excuse for not giving it a shot!</p>
</blockquote>

<p>A few reasons I really like Treehouse are:</p>

<ul>
<li><p><strong>They give you great starting points.</strong> Treehouse has really dialed in the ability to give you a place to start based on what you want to learn. The days of using google to find resources and duct taping them together to curate your own experience are over.</p></li>
<li><p><strong>I could compete with my friends and co-workers.</strong> When you complete a lesson you're awarded points. Who doesn't love being able to level up?</p></li>
<li><p><strong>Choose your own Destiny.</strong> It helps to have a map when you want to get somewhere. Treehouse has created maps organized into courses by topic and tailored tracks.</p></li>
<li><p><strong>Self-guided learning.</strong> You are only limited by your time-constraints and your drive. They don't limit your time or the number of courses you can take at a time, so the power is in your hands.</p></li>
<li><p><strong>You have a community.</strong> Treehouse believes creating and providing a network of support is fundamental. This is the best way to enforce learning.</p></li>
</ul>

<p>Treehouse was gracious enough to give me <strong>five 3 month memberships</strong> to give away to their subscription based website.</p>

<p>I want to make sure these memberships get used well, so I have a few guidelines:</p>

<ul>
<li><p><strong>Commitment</strong>: Whether you're doing one lesson a day or ten lessons a day, what's important is that you've got gumption through the membership and you don't give up.</p></li>
<li><p><strong>Feedback</strong>: I want to know what you liked and disliked about Treehouse when your 3 months is up. I know this feels like being in high school again, but think of it as a way to help Treehouse make the experience better and potential users decide if it's a good fit for them.</p></li>
</ul>

<p>On top of the <strong>3 month membership</strong> I'll help you figure out the initial track to pursue and provide a little support along the way.</p>

<p class="marker">
  <em>To enter for a chance to win a 3 month membership to Treehouse, answer this question in the comments below</em>: <strong>Why do you want to learn to code?</strong>
</p>

<p><strong>The contest has ended. <strike>The last day to submit a response for a chance to win is Friday, January 24th, 2014.</strike> Good luck! Winners are picked at random, so don't feel like you don't have a good enough reason to submit your reason for wanting to learn to code!</strong></p>

<p><em>Be sure to share this with other people who you think would be interested on Facebook and Twitter by clicking the share buttons on the bottom of the window.</em></p>
