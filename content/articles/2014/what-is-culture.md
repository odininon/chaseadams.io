---
date: "2014-05-31"
title: "What is Culture? [Part 1]"
slug: "/2014/05/what-is-culture-part-1/"
aliases: [
    "/posts/what-is-culture-part-1",
    "/2014/05/31/what-is-culture-part-1/"
]
category: "archive"
---

Culture is a word shrouded in mystery. Lately I've asked a lot of people, "How would you define culture?"

The responses are almost always examples of how we express ourselves through our culture, but not culture itself.

Next week I'm going to share what my definition of culture is and what it looks like, but I want to know from you, my peers and my readers: "How would you define culture?"

Share your definition in the Disqus comment section below.
