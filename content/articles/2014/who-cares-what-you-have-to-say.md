---
date: "2014-02-12"
title: "Who Cares What You Have To Say?"
slug: "/2014/02/who-cares-what-you-have-to-say/"
aliases: [
    "/posts/who-cares-what-you-have-to-say/"
]
category: "archive"
---

<p>This question was once a driving force in my life: <em>"Who cares what I have to say?"</em></p>

<p>It bore down on me every time I started to blog, every time I started a thought, every time I started to speak. The most significant factor that played into it was knowing I was a novice at everything I thought I was good at.</p>

<p>Part of me wondered if people would correct me (which someone always would) and another part of me wondered if what I was thinking was so fundamentally human that everyone else had already thought it and moved on with their day.</p>

<p>The thoughts and concepts that trickled past that point arrived at stage two: "Is it well formed enough that I can make a compelling argument for it? Is it sound enough in it's grammatical formation that I won't be judged by English Nerds?"</p>

<p>90% of anything that made it to this point was usually out the window.</p>

<p>At the end of this thought process I'd be left with a few sentences worthy of tweeting, that I might stash for later. If you stepped into my brain for any period of time longer than 5 seconds, you'd realize that's an incredible amount of filtering.</p>

<p>I finally realized: <em>What everyone else had to say or think about what I had to say or think shouldn't have been a factor as to whether I expressed it.</em></p>

<p><strong>This is why I believe you should write.</strong></p>

<p>Your thoughts are important. It shouldn't matter who will read what you have to say. It shouldn't matter what they take away from it. It shouldn't matter if they think you're <em>wrong</em>. What matters is that your thoughts are expressed.</p>

<p>You have a liberty and <em>ability</em> to think and to express those thoughts through words.</p>

<p>If liberty and ability aren't enough reason to express yourself through writing here are a few more:</p>

<ul>
<li><p><strong>You Solidify Your World View.</strong> I find that when I write, I give my world view a more clear picture. It makes my perspective less foggy, and I can cut away the cruft.</p></li>
<li><p><strong>You Will Make A Difference.</strong> I didn't think anyone read what I have to write and if they did, I imagined it was just a nice break in their day. But people do read what I say, and some find encouragement from it. The same will be true for you. What you say matters.</p></li>
<li><p><strong>You've Done Your Part.</strong> If you express your thoughts, especially through blogging, on twitter or Facebook, in public forum, it can never be said that you didn't stand up for the things you believed in. It can never be said that you didn't try. You can know you did your part and be proud of it.</p></li>
</ul>

<p>Teddy Roosevelt said:</p>

<p><em>"It is not the critic who counts; not the man who points out how the strong man stumbles, or where the doer of deeds could have done them better. The credit belongs to the man who is actually in the arena, whose face is marred by dust and sweat and blood; who strives valiantly; who errs, who comes short again and again, because there is no effort without error and shortcoming; but who does actually strive to do the deeds; who knows great enthusiasms, the great devotions; who spends himself in a worthy cause; who at the best knows in the end the triumph of high achievement, and who at the worst, if he fails, at least fails while daring greatly, so that his place shall never be with those cold and timid souls who neither know victory nor defeat."</em></p>

<p>Let me reiterate: <span class="marker"> "who at the best knows in the end the triumph of high achievement, and who at the worst, if he fails, at least fails while daring greatly, so that his place shall never be with those cold and timid souls who neither know victory nor defeat." </span></p>

<p><strong>Write. Speak. Express Your Thoughts. Do so knowing that at best, there is high achievement, and at worst: you failed daring greatly.</strong></p>


