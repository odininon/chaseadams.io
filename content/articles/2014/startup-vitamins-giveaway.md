---
date: "2014-02-02"
title: "Startup Vitamins Review & Giveaway"
slug: "/2014/02/startupvitamins-giveaway/"
aliases: [
    "/posts/startup-vitamins-review-giveaway",
    "/2014/02/03/startupvitamins-giveaway/"
]
category: "archive"
---

<p class="intro">
  <a href="https://www.startupvitamins.com">Startup Vitamins</a> has quickly become my go-to source for coffee mugs, T-shirts and posters with inspirational business and startup quotes. Read on for a 30% off coupon and a chance to win a mug of your choice!
</p>

<p>When I first saw Startup Vitamins in my Facebook feed, I knew that it was going to be the beginning of a long relationship of me giving them money and them giving me awesome products. So far this is totally founded.</p>

<p><img src="https://www.startupvitamins.com/media/spots/Office_64.png" alt="svposterpackage" width="300" height="300" class="alignleft size-full wp-image-452" /> My initial attraction to Startup Vitamins was their ability to take inspirational business quotes and add relevant imagery to give a quote that extra punch. If you've seen a Startup Vitamins ad floating through your Facebook stream, you know what I'm talking about.</p>

<h2>My First Order &amp; A Wow Face</h2>

<p>I placed my first order for a "Move Fast and Break Things" mug for standard shipping, thinking I'd get it two weeks later. Two days later my buddy AP from shipping dropped off <em>two</em> packages at my desk from Startup Vitamins. Needless to say, they gave me a Wow moment.</p>

<p>Not only did they send me the mug I wanted, but they sent me another mug that I gave to my friend <a href="https://www.natewienert.com">Nate Wienert</a> (because like the mug says: he gets 'things' done (the mug doesn't say things, but I digress)) and a sticker. I was immediately hooked.</p>

<div class="group">
  Not long after I placed a second order for three T-shirts and two posters:
</div>

<p><img src="https://www.realchaseadams.com/imgs/2014/01/svposters.png" alt="svposters" width="300" height="300" class="alignright size-full wp-image-453" /> I received the posters first and the T-shirts a week later. They were both delivered way ahead of the estimated delivery date, which was yet another "wow" moment.</p>

<p>The posters are printed on matted printer paper. They looked awesome, but I was unsure about was if they'd scratch easily, so I bought some cheap poster frames from target that at least keep them from getting messed up until I can buy some better frames.</p>

<p>The "Passion Never Fails" poster is so bright and beautiful that it catches your eye as you walk through the office. It gives the blank office wall a really nice pop.</p>

<p>The "The Riskiest Thing is to Take No Risks" poster is a really cool design, but the colors of the background and the cliff the man is standing on are hard to distinguish from each other (maybe it’s the frame I bought), so the value of the imagery is lost.</p>

<blockquote>
  <p>StartupVitamins.com has been gracious enough to give me a coupon code for family, friends and readers for 30% off: STARTUPON</p>
</blockquote>

<p>The t-shirts are printed on American Apparel (which you either geek out about because you know how amazingly comfortable and soft they are or you don't know about yet and will geek out about it after you've worn one).</p>

<p><a href="https://www.startupvitamins.com/products/startup-tshirt-life-is-short"><img src="https://www.startupvitamins.com/media/products/34/tshirt.jpg" width="300" height="300" alt="Life is short, do stuff that matters." class="alignleft" /></a></p>

<p>I've gotten a lot of compliments about all three shirts, with the "Life is Short, Do Stuff That Matters" shirt being the one I get the most feedback about. This mantra is my favorite, and I would wear this everyday if it was socially acceptable, just to serve as a personal reminder.</p>

<p>I will say that as much as I love the "Bootstrapped" shirt, I sweat in it more. For some reason light grey shirts don't just show sweat better, they make me sweat more efficiently. Also, in my industry 'bootstrap' has come to mean 'template css for websites', so there's been some confusion between 'bootstrap' and 'bootstrapped'.</p>

<p>The "Experiment" shirt is awesome, but the colors are dark on dark, so it makes it really hard to see what it says unless someone has time to stop and read it.</p>

<p>From a practicality standpoint, I really liked the posters but I'd say the mugs and T-shirts are going to be Startup Vitamins' bread and butter. The fact that these items are "mobile" makes me a lot more likely to buy more mugs and T-shirts, because I know there's a greater likelihood people will see them and geek out along with me.</p>

<p><strong>When I started this post there was a small selection of T-shirts and mugs, but in the last week they’ve rolled out all designs on T-shirts, mugs, posters and stickers. To me, this is going to be a game changer.</strong></p>

<h2>30% off &amp; A Chance to Win a Mug</h2>

<p>If you think any of this sounds awesome, I totally recommend buying Startup Vitamins products and letting me know what you think. If the price point is just out of reach, <strong>StartupVitamins.com has been gracious enough to give me a coupon code for family, friends and readers for 30% off: STARTUPON</strong></p>

<p class="marker">
  I'm giving away a Startup Vitamins mug, all you have to do to enter for a chance to win is answer the following questions: <em>Which Startup Vitamins quote best describes you?</em>
</p>

<h3><em>I just ask that you don't pick one with swear words, there are lots of other cool sayings!</em></h3>

<h2>Oh, one chance isn't enough? Here are a few ways to get extra entries:</h2>

<ul>
<li><p>In your comment, share an image of the item you want.</p></li>
<li><p>Share this on your Facebook wall or twitter. You can use these templates to share the post on your Facebook wall or Twitter feed. If a friend comments and they add "referred by: <em>john doe</em>" where john doe is your name, you'll both be given another entry! The more referred by's you get, the better your chances of winning!</p></li>
</ul>

<p><strong>Facebook:</strong></p>

<p>*Check out this review about Startup Vitamins mugs, T-shirts and posters and get a chance to win an inspirational mug! https://bit.ly/1bV0F9s</p>

<p>Be sure to put "referred by: john doe" in your comment so we both get extra entries!*</p>

<p><strong>Twitter:</strong></p>

<p><em>Wanna win a @startupvitamins mug? Mention referred by @johndoe for an extra entry. https://bit.ly/1bV0F9s</em></p>

<p><strong>Get extra twitter visibility by sharing this photo:</strong></p>

<p><img src="https://www.realchaseadams.com/imgs/2014/02/twitter-sv-mug-giveaway.jpg" alt="Startup Vitamins Giveaway" /></p>

<div class="marker">
  Contest Ends Friday February 14th, 2014
</div>
