---
date: "2014-02-13"
title: "What's Your Lollipop Moment?"
slug: "/2014/02/whats-your-lollipop-moment/"
aliases: [
    "/posts/whats-your-lollipop-moment"
]
category: "archive"
---

<p>Yesterday I listened to this TED talk on the way home and it hit me pretty hard.</p>

<p><a href="https://twitter.com/nuancedrew">Drew Dudley</a> explains that the title of leader shouldn't be set aside for those extraordinary few who create monolithic events, organizations or communities, but that leaders are simply people who create lollipop moments in the lives of others.</p>

<p>A lot of times I feel like I have to strive to be one of those extraordinary few, when I overlook the little things, like truly listening in a conversation, unintentionally helping others connect or sharing a nugget of truth.</p>

<p>There were two questions that really felt like I needed to lay into my life framework:</p>

<ul>
<li>What am I doing to make room for possible lollipop moments?</li>
<li>What lollipop moments have I not thanked others for creating in my life?</li>
</ul>

<p>Take 6 minutes to watch this and let the truth of it soak in.</p>

<iframe src="https://embed.ted.com/talks/drew_dudley_everyday_leadership.html" width="853" height="480" frameborder="0" scrolling="no" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>


