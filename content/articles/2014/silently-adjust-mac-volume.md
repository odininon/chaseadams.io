---
title:  "Silently Adjust The Volume On Your Mac"
date:   "2014-10-21"
tags: [Mac]
slug: "/2014/10/silently-adjust-the-volume-on-your-mac/"
aliases: [
    "/silently-adjust-the-volume-on-your-mac",
    "/posts/silently-adjust-the-volume-on-your-mac"
]
category: "archive"
---

You know that annoying pop, pop, pop noise your sound adjustment makes when you adjust the sound on your Mac? There's a really easy way to adjust your audio without the computer making it.

Just use __Shift + Audio Key__. The Shift Key temporarily disables the pop.

**Update for Mac OSX Yosemite**

In OSX Yosemite, they've reversed the functionality so that adjusting the volume of your computer with the volue buttons is silent and to hear the click you have to use the Shift key.

If you liked the feedback click without the Shift key, you can revert the settings to pre Yosemite settings by opening *System Preferences*, clicking on *Sound* in the second row, and selecting *"Play feedback when volume is changed"*.
