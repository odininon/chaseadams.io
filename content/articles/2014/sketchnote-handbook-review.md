---
title:  "Book Review: The Sketchnote Handbook"
date:   "2014-10-21"
description: "The Sketchnote Handbook is one of the few books I believe everyone should own and revisit often. It's the definitive guide on how to take notes visually, think laterally and pick the right takeaways from a message, talk or meeting."
slug: "/2014/10/book-review-sketchnote-handbook/"
aliases: [
    "/2014/10/book-review/",
    "/posts/book-review"
]
category: "archive"
---

<img alt="The Sketchnote Handbook" src="https://www.realchaseadams.com/imgs/2014/10/sketchnotehandbook-thumb.jpg" class="align-left" width="200"> [_The Sketchnote Handbook_](https://www.amazon.com/gp/product/0321857895) by [Mike Rohde](https://rohdesign.com/) was one of the most important (and impactful) books I read this year. It's a book about learning to consume spoken word (visual language and caching ideas) and how to convert the information into memorable, impressive, visual "sketchnotes".

Created by the guy who illustrated one of my all-time favorite books, [_ReWork_](https://www.amazon.com/Rework-Jason-Fried/dp/0307463745), [_The Sketchnote Handbook_](https://www.amazon.com/gp/product/0321857895) is an illustrated guide about how to take notes visually.

It's the ultimate guide for information Hackers who want to retain and revisit information that's important and relevant to you.

Here's a short Youtube video [Mike made explaining Sketchnotes](https://www.youtube.com/watch?v=6SKQsULasTg):

<iframe width="560" style="margin:0 auto;width: 560px;" height="315" src="//www.youtube.com/embed/6SKQsULasTg" frameborder="0" allowfullscreen></iframe>

<div class="cf" style="margin-bottom: 1em;"></div>

## The Unboxing Was Awesome

<img alt="The Sketchnote Handbook" src="https://www.realchaseadams.com/imgs/2014/10/sketchnotehandbook-inside.jpg" class="align-right" width="200">

It's very rare that I get a book in the mail and spend the first 10 minutes admiring the cover, thumbing through pages, while enjoying both the feel of the paper _and_ the beautifully crafted pages.

The first thing I realized (and I expected) was that this book was not like any other book on using visual language to convey ideas. It was chock full of low-level explanations on layout styles, font styles, processing information and other super useful concepts, but Mike took it up a notch by **practicing the very concepts he was encouraging the reader to learn**.

Every page was visually stunning.

## So Much Illustration

There wasn't a single page that was printed text (the SKU on the back cover is the only place I could find that didn't use Mike's handrolled fonts). Every page was intentional in layout and design and Mike left me spending minutes on each page turn just soaking up the content of the pages.

When I put the book down after my first read, I walked away **retaining concepts such as types of sketchnotes, active listening, hierarchy and personalization**.

The amount of topics Mike covers in [_The Sketchnote Handbook_](https://www.amazon.com/gp/product/0321857895) is uncanny. Just to list a few:

- The 5 Basic Elements of Drawing
- How to Listen
- Drawing People, Faces & Type
- Structure/Anatomy and Patterns of a Note
- Penmanship
- Key Hacks for Awesome Sketchnotes (The Process)

I have spent a few days over the course of the last few months after reading [_The Sketchnote Handbook_](https://www.amazon.com/gp/product/0321857895) revisiting the content, and I have to say, it's become my go to guide for any time I want to leverage the power of visual note taking.

![Sketchnote Handbook Simple Drawings Example](https://www.realchaseadams.com/imgs/2014/10/sketchnotehandbook-simple-drawings.jpg)

Throughout the book, Mike did an amazing job of always making sure the reader has examples to use as a springboard for trying their hand at the exercise or topic at hand.

My only feedback is that I would've liked to have seen the entire alphabet for Drawing Type Exercises. He gives the first three letters of the alphabet (a, b, c) in lowercase and uppercase format, but the complexity of creating a triple-line letter for the letter "R" or "Q" would have been a lot easier with an illustration to follow.

One of the really cool things that happened for me after reading [_The Sketchnote Handbook_](https://www.amazon.com/gp/product/0321857895) and applying the concepts to my notetaking was that people were interested in seeing my notes afterwards. I'd taken this new found skill and turned it into a way to help others where the way I would've taken notes (or not taken notes at all) before barely even helped me.

I also wanted to revisit my notes, thumbing through the pages of my notebook to look at the work I'd done and remember the experience from whatever I had taken notes from.

I could see Sketchnoting (and using this book) being taught to students at a young age, to encourage them to take notes visually (the way that we process information) as an alternative to the traditional way students are taught to take notes. I'm a huge proponent for giving students a resource for them to enjoy learning, retain concepts and ideas more and feel like they can take notes visually without the possibility of "getting in trouble" (if you were a doodler in school, you've experienced this unfortunate consequence of doodling).

## The Takeaway

This book absolutely deserves to grace your shelf.

I believe we all have the capacity to draw (Mike can prove that through his book and a few minutes of your time) and if you are in a position where you grow through learning information, then you need the concepts in this book to catapult your ability to retain and your desire to revisit that information.

## Who It's For

Anyone who takes notes, wants to take notes or does anything involving learning or growth.

This includes students, anyone who goes to meetings, church goers, conference goers or people who watch [TED Talks](/2014/02/whats-your-lollipop-moment/)...pretty much anyone who is growing, or trying to grow, through taking in information from an external source.

## How to Buy It

<img class="align-right" width="250" alt="The Sketchnote Handbook" src="https://ecx.images-amazon.com/images/I/711G3WkR8DL.jpg"> You should buy the [paperback copy of _The Sketchnote Handbook_](https://www.amazon.com/gp/product/0321857895), rather than the [kindle version](https://www.amazon.com/Sketchnote-Handbook-Illustrated-Visual-Taking-ebook/dp/B00E981K1W/ref=tmm_kin_title_0?_encoding=UTF8&sr=&qid=), for a few reasons:

- The artwork is stunning and deserves to be appreciated
- There's space to practice your exercises
- A lot of time and effort went into the layout and organization of the physical copy of [_The Sketchnote Handbook_](https://www.amazon.com/gp/product/0321857895)
- You can share the physical copy with peers much more easily

If you decide to buy the kindle version, at least consider also buying the paperback version, even if only to become a strong edition to your coffee table collection.

Mike has some really phenomenal sketchnote examples on his [flickr](https://www.flickr.com/photos/rohdesign/) and [instagram](https://instagram.com/rohdesign) accounts and he's one of the most [responsive/engaging authors I've encountered on twitter](https://twitter.com/rohdesign). You can find [blog posts and more sketchnotes at rohdesign.com](https://rohdesign.com/).


<div class="msg tiny sans info">
All outbound product links are to <a href="https://www.amazon.com">Amazon.com</a>. There are no affiliate links on this page.
</div>
