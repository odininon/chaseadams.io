---
date: "2014-02-17"
title: "How Often Do You Read A Blog Post? [Wedgie/Survey]"
slug: "/2014/02/how-often-do-you-read-a-blog-post-wedgies-survey/"
category: "archive"
---

<p>Sometimes I don't like to read blog posts or articles all the way through, which is why I like bullet points and posts that are broken up by large-font, short headings. <strong>What about you?</strong></p>

<script src='https://www.wedgies.com/js/widgets.js'></script>

<noscript>
  <a href='https://www.wedgies.com/question/53037587a19a570200000019'>Vote on our poll for Do you read blog posts and articles all the way through?!</a>
</noscript>

<div class='wedgie-widget' wd-pending wd-type='embed' wd-version='v1' id='53037587a19a570200000019' >
</div>
