---
date: "2014-01-08"
title: "What I Learned From Coffee With A Pulitzer Prize Winning Reporter"
slug: "/2014/01/09/what-i-learned-from-coffee-with-a-pulitzer-prize-winning-reporter/"
aliases: [
    "/posts/what-i-learned-from-coffee-with-a-pulitzer-prize-winning-reporter"
]
category: "archive"
---

<p class="intro">
  Every morning this week I’ve sat with Charles Duhigg. Mr. Duhigg is a Pulitzer prize winning reporter for the New York Times who is fascinated with the science of habits.
</p>

He’s told me stories about the habits that catapulted Michael Phelps to become a multi-gold medalist. He unpacked how CEO, Howard Schultz, architected Starbucks into the world’s most successful service company that sells coffee. How CEO Paul O’Neill took Alcoa, an Aluminum company and made them worth five times what they were worth when he started by simply changing safety habits. How Tony Dungy used the power of belief to rearchitect the success of the Tampa Bay Buccaneers.

Along the way, Charles Duhigg has given me great, structured, practical advice about how to use these stories and research to create and modify the habits in my own life. Did I mention that it was all for less than eight dollars?

This is too good to be true, right? __This is the power of a book.__ With one book, one man was able to organize his thoughts, stories and research all into a format that’s easily consumable and written to maximize the information you glean, at your own convenience. You couldn’t ask for a better scenario — to build a better you — than a book.

If you're looking for a good place to start, I recommend the [reading list I made for 2014](/2014/01/05/reading-list/). You'll find The Power of Habit and 14 other books that can help catalyze your professional growth.

__If you had an unlimited copies of one book to give everyone you met, what would it be?__
