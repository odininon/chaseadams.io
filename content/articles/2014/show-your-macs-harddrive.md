---
date: "2014-01-02"
title: "OSX Protip: Show your Mac’s Hard Drive in Finder Sidebar"
slug: "/2014/01/show-your-macs-hard-drive-in-finder-sidebar/"
aliases: [
    "/posts/show-your-macs-hard-drive-in-finder-sidebar-and-on-your-desktop",
    "/2014/01/show-your-macs-hard-drive-in-finder-sidebar-and-on-your-desktop"
]
tags: [ "MacOS" ]
description: "Learn how to show your Mac's Hard Drive in the Finder Sidebar."
---

_Want to show your Mac's Hard Drive on your desktop instead? Check out my tutorial on how to [show your Mac's Hard Drive on your Desktop.](/2016/12/show-your-macs-hard-drive-on-your-desktop)_

Quick access to your Hard Drive is incredibly useful, especially when you need to browse above your home directory. In this quick OSX protip, I’ll guide you through showing the shortcut to your Hard Drive (and other shortcuts) in your Finder sidebar.

## Show Macintosh HD in Finder Sidebar

![Show Macintosh HD in Finder Sidebar](/img/finder-show-mac-hd.gif)

Start by clicking on the Finder icon in your dock, and a new Finder window should come into focus.

In your Finder you should see a sidebar with Favorites and a list of devices. If you don’t see the sidebar, click on **View** in the application menu at the top and then click **Show Sidebar**.

If you don’t see a **Devices** list in your Finder’s sidebar, click on **Finder** in the application menu at the top of the screen, and then **Preferences** in the drop down menu. This will open a new pane called Finder Preferences. Click the Sidebar icon in this pane and it switch to a list of all the possible items you can show in your sidebar.

Under Devices, click the checkbox next to Hard disks and you should now see **Macintosh HD** in your Finder sidebar.

As you can see, you also have the ability to show shared items and other external devices. I hide almost all the Favorites (with exception of Applications, Documents &amp; Downloads), show all the devices and turn on all shared but Back to My Mac.

