---
date: "2014-08-01"
title: "The Best Advice I Can Give To People Learning To Code"
description: "Don't copy/paste code examples. One of the mistakes I see burn developers, both new and veteran, is that they copy/paste code examples, strings, basically anything copy/pastable."
slug: "/2014/08/the-best-advice-i-can-give-to-people-learning-to-code/"
aliases: [
    "/posts/the-best-advice-i-can-give-to-people-learning-to-code"
]
category: "archive"
---

__Don't copy/paste code examples.__

I'm currently learning [GoLang](https://golang.org/), a statically typed language derived from C, as a way to stretch myself as a developer.

One of the mistakes I see burn developers, both new and veteran, is that they copy/paste code examples, strings, basically anything copy/pastable.

There are a few reasons I _always_ write my code by hand and don't copy/paste:

- It doesn't actually _teach_ me anything.
- In a lot of applications (browsers, for instance) there are extra formatting characters that even though you don't see, get copied and ultimately break your code.
- It requires me to read, write and understand every line.
- Sometimes there are variables you aren't accounting for or things you're supposed to type yourself, and not typing code manually leads to really wonky results that can be painstaking to backtrack through.

__Learning to code, or be an engineer or problem solve (whatever you want to call it) is a _long road_. Long roads aren't travelled with shortcuts.__
