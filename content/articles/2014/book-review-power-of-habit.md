---
date: "2014-01-23"
title: "Book Review: The Power of Habit — Why We Do What We Do In Life and Business"
slug: "/2014/01/book-review-the-power-of-habit-why-we-do-what-we-do-in-life-and-business/"
aliases: [
    "/posts/book-review-the-power-of-habit-why-we-do-what-we-do-in-life-and-business"
]
category: "archive"
---

<p class="intro group">
  “All our life, so far as it has definite form, is but a mass of habits—practical, emotional, and intellectual—systematically organized for our weal or woe, and bearing us irresistibly toward our destiny, whatever the latter may be.” <br /> <span style="float:right">~ William James</span>
</p>

<p><a href="https://www.amazon.com/Power-Habit-What-Life-Business-ebook/dp/B0055PGUYU/ref=tmm_kin_swatch_0?_encoding=UTF8&amp;sr=&amp;qid="><img src="https://ecx.images-amazon.com/images/I/51Ml%2BjD9l3L.jpg" width="324" height="500" class="alignleft" /></a> Charles Duhigg’s, <a href="https://www.amazon.com/Power-Habit-What-Life-Business-ebook/dp/B0055PGUYU/ref=tmm_kin_swatch_0?_encoding=UTF8&amp;sr=&amp;qid=">The Power of Habit: Why We Do What We Do In Life and Business</a>, is one of those rare books that keeps you engaged through stories while giving you a practical, applicable understanding of the inner-workings of habits. Duhigg described it as “a framework for understanding how habits work and a guide to experimenting with how they might change.”</p>

<p>Duhigg starts with a framework: <strong>At the core of every habit is a simple, three part, neurological loop: a cue, a routine and a reward.</strong> As I read through the stories of CEOs, football coaches, athletes, gamblers, and communities, I found myself continually looking through the lens of the loop, a testament to it’s simplicity to memorize and power to apply.</p>

<p>One of the things I enjoyed most about <a href="https://www.amazon.com/Power-Habit-What-Life-Business-ebook/dp/B0055PGUYU/ref=tmm_kin_swatch_0?_encoding=UTF8&amp;sr=&amp;qid=">The Power of Habit</a> was that it examined habits from the perspective of different levels of an individual’s accountability. Duhigg began with the habits of the individual, expanded the scope to the habits of organizations and then expanded the scope again to the habits of communities. Each expansion into a larger set was founded on the learnings from the previous set, which not only reinforced the previous set, it also gave a more holistic view to habits themselves.</p>

<p>The entire book was full of valuable, consumable takeaways but the three that resonated with me the most were: the story of Target’s ‘targets’—an uncanny accuracy at targeting it’s customers during their life events, the power of weak ties and how to restructure bad habits into good ones.</p>

<blockquote>
  <p>"To change people’s diets, the exotic must be made familiar. And to do that, you must camouflage it in everyday garb.”</p>
</blockquote>

<h2>Target: A Household Name That Hits Too Close To Home</h2>

<p>One chapter of The Power of Habit was dedicated to research on how companies and organizations monitor our habits, through collecting data and how they use it to filter their offerings. He unpacks Target’s research in such a way that you realize that the name “Target” is actually pretty accurate when it comes to focusing on, and acquiring, customers (or targets). He explains how Target’s use of data allowed them to target a woman who was pregnant, which trimester she was in, and when the best time to send her coupons for baby items was.</p>

<p>They found that this experience had a potential to be jarring to customers, so they started also sending coupons for items they’d expect: detergents, lawn care products and other hapless seeming items. He explained it as sandwiching the exotic between the familiar: <em>“To change people’s diets, the exotic must be made familiar. And to do that, you must camouflage it in everyday garb.”</em></p>

<p>This chapter was easily one of the most fascinating to read, and even though it didn’t directly touch on habits of the individual, organization or community, it did give interesting insights into how to leverage those habits from a marketing standpoint (which I find interesting).</p>

<h2>The Strong Impacts of Weak Ties</h2>

<p>My favorite takeaway from the book was the concept of “Weak Ties”. He described weak ties as, <em>“represent[ing] the links that connect people who have acquaintances in common, who share membership in social networks, but aren’t directly connected by the strong ties of friendship themselves.”</em></p>

<p>He explores how these weak ties actually have as much impact on our habits as, if not more than, strong ties. He notes:</p>

<p>“The habits of peer pressure, however, have something in common. They often spread through weak ties. And they gain their authority through communal expectations.”</p>

<p>As I read through the section about weak ties, I was constantly drawn to the application aspect of it: How can communities (and organizations) leverage these weak ties to curate their growth?</p>

<h2>The Power of an Appendix</h2>

<p>I really dislike authors who sprinkle information about how to apply their topic through a book and put the responsibility on the reader to collect it all themselves. <strong>This was not the case in The Power of Habit.</strong></p>

<p>Duhigg took all of the “here’s what to do with what you’ve read” and included it as an appendix at the end of the book, not as a “how to” that stands on it’s own, but a “how to” that leverages everything you’ve read to that point. He explains that discovering the routine of a habit is the springboard to change: “with most habits—the routine is the most obvious aspect: It’s the behavior you want to change.” When you discover the routine and the why’s around it (the reward helps with that), the power is in your hands to change it.</p>

<h2>Conclusion And Recommendation</h2>

<p><strong>If you have habits you want to change this book is for you.</strong> If you want to understand how habits shape companies, organizations and communities, <strong>this book is <em>definitely</em> for you</strong>.</p>

<p>I wouldn’t recommend getting it as an <a href="https://www.amazon.com/The-Power-Habit-What-Business/dp/030796664X/ref=tmm_abk_title_0">Audible book</a>, unless it’s as a supplement to the <a href="https://www.amazon.com/Power-Habit-What-Life-Business-ebook/dp/B0055PGUYU/ref=tmm_kin_swatch_0?_encoding=UTF8&amp;sr=&amp;qid=">Kindle edition</a>, <a href="https://www.amazon.com/The-Power-Habit-What-Business/dp/081298160X/ref=tmm_pap_title_0">paperback</a> or <a href="https://www.amazon.com/The-Power-Habit-What-Business/dp/1400069289/ref=tmm_hrd_title_0">hardcover</a>.</p>

<p>Based on some of the concepts of this book I've already modified habits of constantly looking apps that give me data (social, analytics, fitbit) &amp; created habits of reading daily.</p>

<p class="marker">
Do you have a habit that you want to modify? If so, what are you doing to change it? Have you successfully modified a habit? How'd you do it?
</p>
