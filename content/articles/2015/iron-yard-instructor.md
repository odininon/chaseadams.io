---
title: "Joining The Iron Yard as Part-Time Frontend Instructor"
date: "2015-06-29"
slug: "/2015/06/iron-yard-instructor/"
aliases: [
   "/2015/06/frontend-engineering-instructor-at-the-iron-yard"
]
description: "As of tomorrow (June 30, 2015), I officially join The Iron Yard as a part-time frontend engineering instructor."
---

If you have spent any length of time with me, you know that two of the most core principles I hold are __*to pursue growth and learning*__ and __*to pursue thy craft*__.

Knowing that about me, it probably comes as no surprise that I've decided to start teaching my craft.

As of tomorrow (June 30, 2015), I start one of two new journeys: I officially join The Iron Yard as a [part-time frontend engineering instructor](https://theironyard.com/courses/part-time/front-end/)!

<a href="https://www.flickr.com/photos/realchaseadams/19077584548" title="FullSizeRender by Chase Adams, on Flickr"><img src="https://c1.staticflickr.com/1/264/19077584548_f4ea27fd2a_z.jpg" width="640" height="541" alt="FullSizeRender"></a>

## Why Teaching?

I've spent so much time investing in personal growth, I felt it was time to invest in other people more _pragmatically_.

I have made a lot of mistakes and learned a lot through trial and error. I can share those learnings in a way that accelerates a student's growth without the headache.

I've always wanted to teach in some capacity, but I've never felt like the public education system was the place for me.

I never thought I'd find the opportunity to teach where the students are there because they want to be and they truly have a stake in their learning. I never thought I'd find a group of people dedicated to uprsuing a craft.

## Why The Iron Yard?

It's simple: they walk the walk. I first met a few of their founders last year and I've kept up with them and their work and it's undeniable that they are focused on creating growth through technology education.

Their mission statement resonates with me very deeply:

>__*"The Iron Yard exists to create exceptional growth and mentorship for people and their ideas through tech-focused education."*__ .

When I guest lectured at the Atlanta campus, I _saw_ this mission statement being lived out through the staff.

<a href="https://www.flickr.com/photos/realchaseadams/19269022401" title="ironyard by Chase Adams, on Flickr"><img src="https://c1.staticflickr.com/1/309/19269022401_d6940757ca_z.jpg" width="640" height="480" class="full" alt="ironyard"></a>

It felt like the missions statement resonated with everyone I met there  as well and that was when I realized that The Iron Yard was the organization they said they wanted to be.

## The Details

I'll be teaching a class twice a week in the evening, three hours per session, for ten weeks, giving new frontend engineers the tools and skillsets they'll need to dive into a full-time course.

Tucked away further into their [About The Iron Yard](https://theironyard.com/about/) page was this little gem:

>  Our mission is to find people who want to pursue the craft and life-long adventure of technology, teach them the tools of the trade, and then release them into the world with the drive and capability to make a difference. We believe that passionate, talented people who love what they do will create great families, jobs, companies and solutions to the world’s problems.

I want to help others pursue their craft. I want to teach others the tools of the trade. I want to release a tenacious group of people into the world to create a better version than the one we have.

It's going to be a challenging season but I'm excited to take a group of self-motivated, problem solving learners and give them the best opportunities for growth I can.

I'm grateful The Iron Yard is taking a chance on me to help build engineers who __chase their craft__.
