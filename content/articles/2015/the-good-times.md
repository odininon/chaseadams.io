---
title: "The Final Paragraph in My Zappos Chapter"
slug: "/the-good-times/"
date: "2015-07-24"
description: "The good times are the ones you're in."
---

Today is going to be a hard day for me.

I've said most of my goodbyes, I've emailed out my goodbye email, I've even had my goodbye lunch.

The closure won't change the fact that I'll always remember the good times I had at Zappos and the good friends I've made.

To all my friends who were once a part of my Zappos experience and have left: I'm so grateful for everything you did to help me grow, laugh and get through the struggles.

To all my friends who are still there: You're the secret sauce that makes Zappos great. It's not holacracy or "culture events" or free coffee.

To my lunch crew: I'm going to miss the chance to totally zone out from work to talk about something gross or watch videos and looking at pictures of all of you guys when you were kids.

To FEZ: Thanks for all the laughter, candy overloads and cards against humanity. I learned something awesome from everyone of you and I hope I've helped you learn something awesome too. Our code is our craft. Never forget. #fezForLife

To GSD: You guys have been one of the most awesome groups of people, both the alums and the people who are still with us and I am so proud of you guys for all the work you did to get Search and Product pages out the door despite the hurdles we had.

To GSD Contractors: You guys were as good as full-time hires as far as I'm concerned. You were awesome Zapponians.

To mdot: Rafael, you've got one of the best attitudes of anyone I know. Someone could ask you to clean a toilet and you'd figure out how to make it fun and do it with a grin the entire time. Thanks for always reminding me that work is fun when you're with people you enjoy being with. Dave...well...you know.

To Mark Walker: I'll never forget the first time we played ultimate frisbee, built FRACK for 6pm.com and somehow always ended up on the same teams. I'll miss our walks to the water cooler to watch drug deals go down at the pump and snack, arguing over code review practices and letting you win, running after work those few times we did it and your genuine acceptance of the fact that I believe something totally different from you and all of the really wonderful conversations that came from it. Regardless of how much you want people to think you hate people, it's very transparent that you're one of the most thoughtful, kind and helpful people that anyone can ever know. And, just for you...

I think Andy said it best..."I wish there was a way to know you're in the good ole days, before you've actually left them."

<iframe src="https://www.youtube.com/embed/C7qcFCTa1vw" frameborder="0" allowfullscreen></iframe>
