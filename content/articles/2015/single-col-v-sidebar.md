---
title: "Which Blog Layout Do You Prefer: Single Column or Two Column with Sidebar?"
slug: "/2015/07/single-column-vs-sidebar/"
date: "2015-07-28"
category: "archive"
---

When it comes to blog layouts, which do you prefer single column or two columns with a sidebar?

Personally, I go back and forth as to which I prefer, but I'm interested to hear what other people think.

Cast your vote with the [Wedgie](https://www.wedgies.com) below or comment in the Disqus thread if you feel like your response needs further explanation!

<script src="https://www.wedgies.com/js/widgets.js"></script><noscript><a href="https://www.wedgies.com/question/55b785f598e8b20d00001900">Blog layouts: Single column or two column w/ sidebar?</a></noscript><div class="wedgie-widget" data-wd-pending data-wd-type="embed" data-wd-version="v1" id="55b785f598e8b20d00001900" style="max-width: 640px; margin: 0px auto; width: 100%;"></div>

Depending on the results, I may swap back to a single column layout or further tweak my two column layout.
