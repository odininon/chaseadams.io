---
title: "6 Ways To Celebrate Your Employees The Zappos Way"
date: "2015-07-15"
slug: "/2015/07/celebrate-your-employees/"
description: "Celebrating your employees can easily one of the best morale boosters for your organization. Here are 5 of the ways Zappos has done that for me."
---

[As I'm leaving Zappos to go to Walmart Labs](/2015/07/walmart-labs), I've been reflecting on some of the great qualities I appreciated over the years. One of the qualities that comes to mind over and over is the value people put on celebrating employees for their time, effort and work.

In this post, I'm going to highlight 6 ways that have resonated with me most. The crazy thing is, they have nothing to do with compensation and everything to do with **letting people know they're valued**.

<img alt="Celebrate Your Employees" src="/img/celebrate-your-employees.jpg" class="full" />

## Welcome Them During New Hire Graduation

When you join Zappos, _generally_ you go straight into New Hire Training: a month long endeavour to learn about the company history and get a better grasp on _our_ customer. On the last day of New Hire Training, we have a graduation where Zapponians pack the room, bring noise makers and hoot and holler for the new team members that will join their teams on the following Monday.

When I joined Zappos, I was on a team called "FEZ" (Frontend @ Zappos) and it's customary to give every new FEZZER a FEZ hat. I still have mine and plan on keeping it even though my time with Zappos is over.

I'm not advocating for a month long training course for all new employees, but having a big batch of employees start on the same day of the week/month and welcoming them with a big group of people is a really great first experience.

## Celebrate their anniversary

During New Hire Training, you get a temporary license plate to hang over your desk and Zappos honors your first year by replacing your temporary license plate with a real license plate with your name on it. Most teams will go to lunch to celebrate an anniversary and every anniversary generally comes with some sort of swag. Also...cake. Every anniversary comes with cake and a big shout across the floor: "Hey everybody, it's [fill in the blank]'s 5 year anniversary today!"

## Celebrate their hard work

This may seem like a no brainer, but it's really easy to glaze over celebrating hard work. In my opinion (and in my experience), this isn't done very well at a high level at Zappos, but leadership within my department has always done a great job of celebrating the work we've done.

It makes it a lot easier to get through the muck when you know that the people asking you to do it care.

**Note: Even if the work doesn't result in the high level desired outcome, it doesn't mean the people didn't do the best they could with the tools you gave them, so don't use "they didn't meet the results" as an excuse.**

## Co-worker Bonuses

...because your peers are the people who know who's working hard.

This one is probably my favorite: At Zappos everyone has a $50 co-worker bonus to award to someone (per month) who's "Wowed" them. You may think to yourself, "that seems gameable!" but I think it's the most under-utilized tool we have. One of my favorite things to do is to give co-worker bonuses to somebody who's pushed hard to wow their team. When I receive them, it's one of the best ways for me to feel validated in the work that I've done.

## Ask them what problems they're facing...and do something about it

I'm getting on a high horse with this one: I've had some really amazing conversations with leadership for my teams and Tech and seen some great changes.

If a leader says, "what's holding you back?" or "what problems are you facing with xyz?" and an employee says, "abc is a problem." **do something about abc.**

## Sprint Awards

My team does two week sprints (which is a "regular, repeatable work cycle") and at the end of every sprint, someone's given the sprint award based on team nominations followed by a vote. We usually get a plush character (in the past we've had Luigi and Bowser, currently we have Stuart the Minion) and the winner gets to keep it on their desk for the sprint.

It might sound kind of silly (and it is because my team is awesome like that) but it's a great reminder that the team appreciates that individual. If you're on a team but you don't have a regular "cycle" for work, you could still have a 2 week celebration where a team member is awarded with a "we love you, you're awesome." award.

<img src="/img/stuart-award.jpg" alt="The Stuart Sprint Award" class="full" />

## A Final Thought on The Weight of Leadership

One major takeaway here is that if you're a leader, your "title" matters for how people are going to receive a lot of these examples. For instance, if you are a CEO and you email an employee to say, "hey, I really appreciate the work you've done" (and mean it), it's probably going to carry a lot more weight than if I (as a peer) did it.

**Even if you're organization is "flat", the more recognized you are as a leader in the organization, the more weight your actions carry.**

_What are some ways you appreciate about how your organization celebrates employees?_
