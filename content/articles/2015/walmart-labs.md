---
title: "Part 2: Joining Walmart Labs as Senior Software Engineer"
date: "2015-07-01"
slug: "/2015/07/walmart-labs/"
description: "As of July 29, I'll be a Senior Software Engineer at Walmart Labs"
---

If you missed the first "Big News" post from earlier this week, I started [teaching Frontend Engineering for the Iron Yard part time](https://chaseadams.io/2015/06/frontend-engineering-instructor-at-the-iron-yard/). This confused some people, because they thought I was leaving Zappos but didn't understand why I was only leaving to do part-time work.

Starting July 29, I'll be joining [Walmart Labs](https://www.walmartlabs.com/) as a Senior Software Engineer on their Global Products team.

Some reasons I'm super stoked about this opportunity:

**I get to work remote.** Walmart Labs has offices in both Mountain View and San Bruno, California, but the team I'm joining is fully remote, which means I'll be working from home. We'll stay in Vegas for another seaason and in the meantime figure out where we want to go!

**I get to work on technologies I love.** Walmart Labs created my favorite Node Server framework (Hapi) and they're doing an entire rearchitecture of their website around [Hapi](https://www.hapijs.com), [React](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&ved=0CB4QFjAA&url=http%3A%2F%2Ffacebook.github.io%2Freact%2F&ei=AfOTVff0Bo7GogTI54PYCw&usg=AFQjCNHdWiPEYmXAnso1kFmH1X65GT26Iw&sig2=GqP4LF_1iONti3kG77U-Qw) and [Flux](https://facebook.github.io/flux/) (if these words don't mean anything to you, think Candy, popcorn, cream soda. That's how much I enjoy working in these technologies).

**I get to work for a company who strives to innovate in technology.** Zappos is an interesting place and I believe they want to innovate in a lot of spaces, but technology is not one of them. Walmart Labs has been steadily building better customer experiences, solving interesting problems and contributing to the open-source community heavily. These three things interest me very much.

I'm going to miss my teams at Zappos, and I'll miss the relationships I had the chance to build, but a lot has changed since I started there. I did my part to try and fix the things that felt broken and I at least feel like I gave it my best effort.

More than anything, I'm just incredibly excited for the next season of life: teaching frontend engineering and building stuff that propels the web forward. I couldn't ask for more than that!
