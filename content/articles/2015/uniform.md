---
title: "Separation of Work & Life"
slug: "/uniform/"
date: "2015-07-10"
description: "If you haven't burnt out already but you're working non-stop, I imagine burn out is close. These are the steps I took to get my life back."
---

It was 4:30 AM. I knew I had goals for the morning, but I thought to myself, "checking my work email won't hurt. Surely no one has emailed me between 9 PM last night and this morning." The next thing I knew it was 6:30 AM and I'd spent most of my morning reactively responding to work emails.

_Work_ emails.

That morning I knew it was time to declare a separation of work and life...but I had no idea where to even start.

Fortunately, my buddy Thai, a guy who I've looked to for honing my personal operational rigor, had been dealing with the same problem. That same morning we sat and had coffee on the bistro patio at Zappos and had a conversation about how to create that separation. These solutions are directly or indirectly inspired by my conversation with Thai ([He has an awesome blog about learning ruby](https://thaiwood.io/) and is working on a book about [system adminstration with ruby](https://thaiwood.io/books/ruby-for-system-administrators/)).

## Create A Work Uniform

The first key that almost seemed obvious to creating the separation between work and life was **"create a uniform"**. It didn't have to be a "true" uniform just a token that represented that I was "on", I was at work.

Thai worked as an EMT for a while, and one of the things he had to do was to wear a standard uniform. I could see there was a definite case for having some kind of consistent thing that when you put it on, you're at work and when you take it off, you're in your life.

The funny thing is that Zappos practically hires people because they hate uniforms or rules, which might point to why many of my peers either burn out or work so hard that they're just spinning their wheels.

For me, creating a uniform actually turned out to be pretty simple. I found a [silicone wristband](https://www.reminderband.com/) that I keep in the car and as soon as I get to my parking spot at work, I shoot Jackie a text to let her know I'm in the office and done my silicone wristband. I try to take it off as soon as I get back to my car and at the latest when I get to my driveway.

![usb wristbands](https://ecx.images-amazon.com/images/I/31SJyHRxumL.jpg) If you're in an industry where you use a USB, there are also some cool [USB wristbands](https://www.amazon.com/Wristband-Flash-Memory-Drive-White/dp/B00HSW85V4/ref=pd_sim_147_6?ie=UTF8&refRID=1D1M8X1193AR8Z33V0JH) that could double as a uniform aid and storage solution. I might try this out, because I'm all about minimalist solutions.

An interesting anecdote after practicing "wearing a uniform" for a few weeks: I forgot to take it off one day and I was "tempted" to check my email...and only then did I realize I still had it on! I immediately took it off and forgot to check my email.

## Create A Work Routine

<img class="full align-none" title="working from the patio" alt="working from the patio" src="/img/patio.jpg" />

When I get into the office, I have a routine that involves taking the stairs to the second floor to grab a cup of coffee and two bottles of perrier, heading out to the Zappos Cafe patio and writing documentation or working on an internal project and finally making my way up to the 5th floor. I used to do this before team standup (which was at 10 AM), but now that we have a virtual standup (which is going _really_ well), I just try to get to my desk around 10AM instead of just before.

I try really hard not to check my email before 10 AM, so that I can be proactive about the first hour of my day. I find that email is almost always a great way to derail you from the tasks _you've_ decided are important to satisfy someone else's tensions...which keeps you from getting the important stuff done.

At the end of the day, I make some notes about how I spent my day in and how I could have spent it better in [DayOne Journal for Mac](https://dayoneapp.com/). I then shut down all the applications on my work laptop and shut my computer off. I don't turn it on until the next morning when I start my routine over again.

## Timeblocks & Tasklists

I have a set amount of time that I'm willing to work, with buffers on both start and finish and a _reasonable_ list of things I know I need to get done that day. Blocking your time and having a hard limit to how long you'll stay at the office is probably the most important component of all. Being able to create limits is a difficult discipline, but it's one worth doing.

Do I stay late sometimes? Absolutely. A few weeks ago I had to stay well past my normal hard limit four nights a week, but I knew it was coming because of the gravity of the project I was working on. I let Jackie know at the beginning of the week that I was going to be late getting home every night and she was totally fine with it. I'd like to think that it's because I've been better about guarding my time lately.

One last thought on hard limits and breaking them: If you stick to your hard limits, when you do break them, make it count. Choose to stay late for _good_ reasons, otherwise people are going to (both inadvertenly and intentionally) take advantage of that.

## Liberate Your Life From Work

If you've been in the workforce for any period of time, I don't think I need to tell you how hard it can be to separate work and life. It's even harder when you work at a place like Zappos where it's almost expected that they co-exist (happy hours, unofficial events outside of work that may or may not impact how others view your performance, working on "passion" projects on nights and weekends).

If you haven't burnt out already but you're working non-stop, I imagine burn out is close. You don't have to work all the time to be a machine of productivity.

You might be thinking to yourself, "this guy is an idiot, he has no idea what he's talking about." I, myself, have been there. That's okay. I think there are seasons when we just go full-throttle and we can handle burning the candle at both ends (and those are the seasons we should be chasing the hustle). But I'd like to encourage you, that when you do hit your wall, that you come back and revisit these keys for work-life balance and try and implement at least one of them.

_So what about you? Do you have triggers for how to separate your work/life balance? Do you think work/life balance is bunk in the first place? I'd love to hear your thoughts!_
