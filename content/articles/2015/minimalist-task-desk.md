---
title: "Rustic Industrial Minimalist Task Desk"
date: "2015-07-12"
slug: "/2015/07/minimalist-task-desk/"
description: "A minimalist task desk for under $300 and built in under 30 minutes."
category: "archive"
---

As much as I enjoyed working on and using my standing desk, it wasn't built for full-day usage (it wasn't as stable as I need for programming 6+ hours a day). I told Jackie I wanted to look at desks since I'll be working from home for Walmart Labs full time, so we started the quest for the right desk.

We both have high standards for how a desk should look and we both really like the rustic industrial style, so we set out to find something inexpensive (less than $500) and stylish. Just like with the quest for the right standing desk...nothing.

So, in our typical semi-bespoke style, we put together a plan to build a desk on our own!

Here's how we built this beautiful sitting desk and the best part about it? It only took us about **20 minutes to assemble**.

<img alt="Full Front View of Desk" src="/img/minimal-task-desk/full-front-desk.jpg" class="full" />

## Parts

- [28" Raw Steel .375 Diameter 3 Rod Hairpin Leg ($20/leg)](https://www.hairpinlegs.com/products/3-rod-hairpin-leg?variant=1125171392)
- [74" x 1-1/2" KARLBY Countertop, Walnut ($189.00)](https://www.ikea.com/us/en/catalog/products/10301149/#/30301148)
- [Everbilt #10 x 1" Flat Head Phillips Wood Screws](https://www.homedepot.com/p/Everbilt-10-x-1-in-Zinc-Plated-Flat-Head-Phillips-Drive-Wood-Screw-100-Piece-801882/204275500)
- [3M Knob Adjust Keyboard Tray, Standard Platform, Gel Wrist Rest, Precise Mouse Pad, 17 in Track, Black](https://www.amazon.com/gp/product/B001B0DCOO/ref=as_li_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=B001B0DCOO&linkCode=as2&tag=thechasceperb-20&linkId=XYJLDXHUAVDOJ24F)

## Building the Desk

**Note**: Make sure to _not accidentally drill through the table top!_

**Flip the table!** Put the table top upside down on a soft surface (carpet) and make sure you leave plenty of space around the table top so you can move around it easily.

**Pre-drill the holes.** One thing my dad taught me a long time ago was pre-drill holes. It'll keep wood from splintering or splitting and make it a lot easier to drill the screws in. I measured so that the leg measured 1-1/4" from the edges and held the leg while Jackie marked where the holes needed to be pre-drilled. We pre-drilled three holes for all four legs.

**Attach the legs with fasteners.** Align the legs over the holes and drill the screws into the pre-drilled holes.

**Pre-drill holes for the 3M Keyboard Tray Mount.** Place the tray 1-1/4" away from the front edge of the desk. I measured from both side edges to make sure the mount is exactly the same distance from each edge. Mark the holes on the mounting with a marker, remove the tray and pre-drill the holes.

**Attach the 3M Keyboard Tray Mount.** Place the tray back over the pre-drilled holes and attach the mount to the desk.

**Assemble the Tray.** Follow the instructions to assemble the tray to the mount.

**Attach the Powerstrip.** My good friend Brandon Murry asked on my last project how I was planning on hiding all the power cords, so I tried to be more concious of it this time. I mounted the powerstrip to the bottom of the desk and used a combination of [mug hooks](https://www.amazon.com/ARROW-160376-Hooks-Rubbed-Bronze/dp/B00VEDF6I8/ref=sr_1_3?ie=UTF8&qid=1436750212&sr=8-3&keywords=mug+hooks) and [3m cord clip cable organizers](ttp://www.amazon.com/gp/product/B00M9FN2KY/ref=as_li_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=B00M9FN2KY&linkCode=as2&tag=thechasceperb-20&linkId=UIFHXUV6OHQCWYXW) which made the cables "virtually" invisible without you crawl under the desk. Even if you crawl under the desk, they look a lot better than they did with the standing desk.

The most "involved" part of building this desk is assembling the tray for the keyboard, but we were still able to put it all together in under 30 minutes.

## Finished Task Desk

### Full View of Desk

<img alt="Full View of Desk" src="/img/minimal-task-desk/full-desk.jpg" class="full" />

### Full View of Hairpin Leg

<img alt="Full View of Hairpin Leg" src="/img/minimal-task-desk/legs.jpg" class="full" />

### Attached Hairpin Leg to Desk Top

<img alt="Attached hairpin leg to desk top" src="/img/minimal-task-desk/attached-leg.jpg" class="full" />

### Attached Tray

<img alt="Attached tray to desk top" src="/img/minimal-task-desk/attached-tray.jpg" class="full" />

### Attached Powerstrip

<img alt="Attached power strip to desk top" src="/img/minimal-task-desk/power-strip.jpg" class="full" />

_Have you built a desk like this? If so, be sure to share photos!_
