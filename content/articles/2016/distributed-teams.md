---
title: "Remote is Dead. Long Live Distributed."
description: "The term 'remote' focuses on where the team member is. It doesn’t address what needs to be done or how to do it. The where is totally irrelevant to the work. The term 'distributed', however, naturally lends itself to thinking about what needs to be done and how we go about doing it together. I am not a remote worker, I am a part of a distributed team."
date: "2016-11-15"
slug: "/long-live-distributed-teams/"
---

I am _not a_ remote worker, **I am a part of a distributed team.** 

The word “remote” is loaded with negative baggage: something or someone that’s far away, distant; isolated. Something or someone that’s out of touch with the reality of how things really are.

As a software engineer, when I think about the term “remote”, I think about accessing a machine that’s out of sight, but one in which I can control. It’s uni-directional, communication is often flaky at best and creates frustration in the ability to be effective.

The term “remote” focuses on _where the team member is_ . It doesn’t address _what_ needs to be done or _how_ to do it. The _where_ is **totally irrelevant to the work.**

The term “distributed,” however, naturally lends itself to thinking about **what needs to be done and how we go about doing it together.**

Communication on a distributed team isn’t uni-directional, it’s not even bi-directional, it’s **n-directional** , as each individual now decides how they collectively work towards a common purpose.

An organization shifting from having remote workers to **the** **distributed model helps teams transcend the “remote vs in-office” mentality**. In a remote model, it’s common for those who are in an office (which may very well be remote from the home office) to ask, “why does that person get to work from home (or co-working space) and I have to be in the office?” It’s also common that a remote worker feels they’re left out of the loop because communication is done in person.

Shifting from thinking about people as “remote” to teams being distributed teams allows us to streamline communication, create stronger connections, expand the candidates we hire so we find the best fit for our teams, not just “local” talent.

 **The distributed team paradigm focuses on…** 

- _how_ we work being the driving force to our success (not _where_ we work)
- goals being concrete and achievable
- communication being clear and focused
- individual self-management equally contributing to the team’s success

 **The distributed team paradigm is _not…_** 

- an opportunity to shirk responsibility
- a way to get out of being a part of a group
- the chance to get out of meetings or being around people

 **The distributed model** allows us to do our best work when our bodies and minds are most optimal, rather than feeling the need to be “butts in seats from 9–5 to be noticed.” As our technologies become more distributed (as they already have), our _teams_ becoming distributed facilitates our abilities to think of how our software works (and can be better) and how our software works helps us shape the way our teams effectively move our products towards their ultimate purpose.

So this is a call to join us in our journey to eliminate the idea of remote workers and **embrace the idea of distributed teams** . Over the next few months we’ll be posting retrospectives of what we’ve done and how it worked for our teams. We invite you to join us in doing the same and using the tag #distributedteams and sharing what you’ve tried, what worked and what didn’t go so well.

We are not remote. We are not in-office. **We are Distributed.**