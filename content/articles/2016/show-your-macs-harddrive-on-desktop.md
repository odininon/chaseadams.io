---
date: "2016-12-04"
title: "OSX Protip: Show your Mac’s Hard Drive On Your Desktop"
slug: "/2016/12/show-your-macs-hard-drive-on-your-desktop/"
tags: [
"osx protips"
]
description: "Learn how to show your Mac's Hard Drive on it's Desktop."
---

If you want to show your Mac's Hard Drive in your Finder sidebar instead, check out ["Show your Mac's Hard Drive In Finder Sidebar"](/2014/01/show-your-macs-hard-drive-in-finder-sidebar).

Quick access to your Hard Drive is incredibly useful, especially when you need to browse above your home directory. In this quick OSX protip, I’ll guide you through showing the shortcut to your Hard Drive on your desktop.

## Show Macintosh HD on Desktop

![Show Macintosh HD on Desktop](/img/finder-show-mac-hd.gif)

Open Finder and click on “Finder” in the application menu at the top of the screen, then “Preferences” in the drop down menu. This will open the Finder Preferences pane.

If “General” isn’t the selected navigation icon (for instance, if you just finished the Finder Sidebar tutorial), select it.

Under “Show these items on desktop” click on “Hard disks”. Now if you look at your desktop, you’ll see the icon for your Macintosh HD.
