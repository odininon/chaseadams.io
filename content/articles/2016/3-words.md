---
title: "My 3 Words for 2016"
description: "My 3 Words for 2016: Discovery, Rigour, Systematic"
date: "2015-12-23"
slug: "/2016/3-words/"
aliases: [
	"/my-3-words-2016"
]
category: "archive"
---

This year, I'm picking 3 Words to act as targets for 2016.

_My 3 Words_, originated in 2006 by Chris Brogan, is an alternative paradigm to creating New Year's resolutions. In his 2013 post, Chris explains how _My 3 Words_ fits into your life planning:

- The big story.
- Vision.
- Goals.
- Plans and Milestones.
- The daily calendar.

~ [My 3 words for 2013 — chrisbrogan.com](https://chrisbrogan.com/my-3-words-for-2013/)

Essentially, rather than creating reactive, rigid (and often myopic) resolutions, pick 3 words that help move your life story forward.

The concept of _My 3 Words_ resonates deeply with me for a few reasons:

- **Proactive.** Often New Year's resolutions are picked due to a _current_ dissatisfaction (often to do with health or fitness and because someone ate too much stuffing and pie). My 3 Words takes the _big picture_ into consideration.
- **Flexibility.** New Year's resolutions are often too rigid (not to be confused with _rigour_), which is the main cause of most broken resolutions.
- **Encompassing.** New Year's resolutions often only focus on specific areas. _My 3 Words_ creates space for application in any and all areas of life.
- **Forgiving.** Missing a day in a resolution, breaks that resolution. _My 3 Words_, because of its flexibility and ability to apply in any area of life, only requires awareness of the day's activities through the lens of the 3 words.

What I appreciate most about the practice of picking 3 Words is that it makes them easy to remember, easy to gut check and easy to reason about...**and easy is the major catalyst for follow-through**.

## My 3 Words for 2016

So, 2016, here are my 3 words:

- Discovery
- Systematic
- Rigour

Below I've outlined what they mean, why they're important to me, and specific action items.

### Discovery

_the act of finding or learning something for the first time._

We live in an incredible age: Accessibility to _everything_ has never been so abundant. I feel a great deal of excitement in discovering new things, people, places, ideas and processes. Even more exciting is discovering new aspects in what I already know!

- seeking out new adventures
- knowing God more
- knowing myself more
- knowing Jackie more
- learning new processes, systems and skills
- finding new places

### Systematic

_done or acting according to a fixed plan or system; methodical._

I've found that the tasks and areas of life I'm most successful in are the ones where I understand, fully, what they mean, how they relate to me and how to act. Creating processes for repeatable tasks and then either (1) documenting them or (2) automating them will allow me to be prepared to pivot in the case that I need to. **In structure, there is freedom.**

- discovering personal processes that make me effective
- everything has a place and in its place
- documentation
- automation of those things that can be automated

### Rigour

_the quality of being extremely thorough, exhaustive, or accurate._

In software, I've noticed the difference between a moderate engineer and an excellent engineer is that an excellent engineer reviews their own code. They may take a little longer to turn out a feature or a bugfix (this often isn't true, but I'm giving the moderate engineer the benefit of the doubt), but those features and bugfixes are rarely, if ever, revisited. They're accurate because they were exhaustively considered.

Moderate engineers can get the job done, but often do so by sacrificing being thorough, which leads to someone else fixing the same problem down the line, with little knowledge of how it was fixed originally.

In all areas of life, I want to be an "excellent engineer".

- triple checking
- understanding things more deeply and fully
- opt for prudence & excellence over momentum
- consider all tangents, contingencies and angles

## A Final Thought: Know Your Destination

I'm excited to see how this thought experiment guides the next year, but I believe it will only have value because I've spent some time ["beginning with the end in mind"](https://chaseadams.io/2013/10/my-eulogy/), a habit outlined by Steven Covey. _If you're considering the **My 3 Words** exercise, I recommend first starting with knowing (and documenting) the end you have in mind._

Your "end" may change a little along the way, but having a point of reference is an important component of being able to pick the right words to be most effective in your journey.

_Did you pick 3 Words this year? If so, I'd love to know what they are!_
