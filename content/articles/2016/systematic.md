---
title: "A Systematic Approach to Managing Oneself"
description: "This is a systematic approach at applying 'Managing Oneself' to my life. Many of the high-level concepts in this document are heavily borrowed from Peter F. Drucker's Managing Oneself."
date: "2016-11-11"
aliases: [ 
    "/systematic/intro"
]
slug: "/systematic-approach-to-managing-oneself/"
---

**I want to be the best person I ever manage.**

This is a systematic approach at discovering and documenting who I am. Many of the high-level concepts in this document are heavily borrowed from Peter F. Drucker's _Managing Oneself_.

## Table of Contents

- [Introduction](#introduction)
- [Desired Outcomes](#desired-outcomes)
- [What Are My Strengths?](/strengths)
- [How Do I Perform?](/perform)
- [What Are My Values?](/values)
- [Final Thoughts](#final-thoughts)

## Introduction

I believe I can be systematically discoverable.

When I left Zappos.com 4 months ago to work with Walmart Labs, there were many aspects of my professional life that changed and these changes catalyzed a movement in me to systematically discover who I was.

- I transitioned from being "required" to be in an office to being fully distributed & outside of an office
- I transitioned from being on a team with a project manager and sprints to a team that was fully autonomous and self-driven, and for a lack of a better term, was continuously delivering
- I transitioned from a reactive sense of responsibility to a proactive thoughtfulness
- I transitioned from the customer being my customer to engineers who served the customer's needs as being my customer

As is true of any new venture, I felt ill-equipped to successfully make these transitions with the toolset I had. By some strange twist of Amazon recommendations, I stumbled onto _Managing Oneself_ by Peter F. Drucker. **This is [one of three books that I recommend to anyone wanting to chart a path for being an impactful and effective person](/essential-books).**

One of the excerpts that stood out most to me was:

> "Successful careers are not planned. They develop when people are prepared for opportunities because they know their strengths, their method of work, and their values."

The key to success isn't planning, it's knowing _yourself_.

This documentation is the outflow of me attempting to know myself, in a pragmatic, systematic way, based on insights from _Managing Oneself_.

## Desired Outcomes

There are a few desired outcomes I have for doing this, in descending order of importance:

- **Capturing & Understanding Myself.** Ultimately, this guide is for me. I want to have an understanding of myself so I can make better, more prudent decisions based on what I've discovered. Whether it's "should I take this professional opportunity" or "should I engage in this in my freetime", this document and systematic discovery should guide me. If no one else ever looked at it, it would still be worthwhile.

- **Sharing Myself with Peers & Future Peers.** I know that if I work with (or for) someone directly, I want them to be the kind of person who wants to know these topics about me. I want to provide as much information as I can about myself so that I'm (1) a respected and impactful peer, (2) effective in the work where I collaborate and (3) available to be used for the clearly defined strengths, in the method that I do it best, while fully aligned with my values.

- **Sharing The Framework with Others Like Myself.** When I realized this project was a priority, I had no idea where to start...even before I realized it was a priority, I put off making it a priority for that very reason. I want to provide not only what I find about _myself_ by using this framework, but the framework itself. It would be exciting for me to have peers who say, "I took a systematic approach at documenting myself too!" If this document can help one other person, that is a huge win for me.

## Framework

The framework is actually really simple, and as I said before, is heavily borrowed from _Managing Oneself_. As the book itself is really more of a long-form essay (you can find a free [pdf copy here](https://www.usb.ac.za/Common/Pdfs/usb-career-center/articles/HBR%20Managing%20Oneself.pdf)), this is an abstraction of what Drucker suggests any thought-worker should know about themselves and what process I went through to actually figure these things out.

### Strengths, Method of Performing & Values

The first task Drucker recommends is to answer the following questions:

- What are my Strengths?
- How Do I Perform?
- What Are My Values?

#### What Are My Strengths?

This one can be _really_ hard to figure out on your own, fortunately, there's a framework that exists to help you do that already!

I used [StrengthsFinder 2.0](https://strengths.gallup.com/110440/About-StrengthsFinder-20.aspx). If you buy the book, there's a code to take the StrengthsFinder assessment in the back and IMO, the introduction is worth the price of the book and the rest of the book makes a great reference after you've taken the assessment.

You can see the answer to ["What are My Strengths?" here.](/strengths)

#### How Do I Perform?

There are two questions here:

- Am I a Reader or Listener?
- How Do I Learn?

The first one is an easier question to answer: think about the times you've solved a problem or created a mental model of something. Did you do it by reading from texts and documentation or did you do it by having a conversation with yourself and others?

How Do I Learn was a little trickier, so I deferred to the experts and found a questionnaire on [vark-learn.com](https://vark-learn.com/the-vark-questionnaire/). It's a free test and the feedback is given to you on a scale for four categories (VARK):

- Visual
- Aural
- Reading/Writing
- Kinisthetic

Once you've taken the questionnaire and get your feedback, there's lots of great information on:

1. how to intake information
2. how to successfully take notes and
3. how to successfully perform in any test, assignment or examination.

I think 3 could also be general output, as I don't / haven't done any of those 3 tasks in a long time, but I believe how to best output would be great for preparing for meetings, deep-dives & presentations.

Taking this assessment was really helpful in giving me the information I need to know how I consume information to best perform at work and explained a lot about why I do my best work when I interact with people.

### What Are My Values?

This was probably the _trickiest_ one of all because there's no "discovery" framework that I could find that made it really easy to answer questions and get a response with answers about who you are, so I decided to do a self-inventory over time of what really mattered to me in the moment. 

For 3 weeks every time I felt excited about something or really frustrated about something, I'd write it down. Whether that was at work or at home, I'd think about the "event" and write down some abstraction about what it exemplified. 

As an example let's take "Do. Document. Distribute. Automate": Whenever I was able to automate a process that I used to do by hand, it'd put me on top of the world, so I wrote down "automate". Automation, however, was not the whole picture, it was the ability to start with something and work through the process of documenting it, having others help with it and eventually automating it so we could do _other things_ that were _more important_.

I did this for around 30 different phrases and then sat with those, removed many and prioritized the others as "inside out", meaning the values that impacted other values were at the top. "God foremost, Family first" is the lens through which everything els is filtered. After that "Glow In The Dark" because if I wasn't pursuing a life where I was glowing in the dark, the rest didn't matter.

## Final Thoughts

Using this framework to discover who I am has empowered me with one of the most important tools I could ever have: **To know myself.**

Knowing myself and having it documented allows me to revisit it so that I can grow in my strengths, challenge my values and use how I perform to be better in all areas of life. It allows me to give my current and future peers a look into who I am so they can decide if I'm a good fit for their team or organization and **give others a recipe for discovering who they are so they can _glow in the dark_.** 
