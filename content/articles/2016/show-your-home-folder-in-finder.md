---
date: "2016-12-05"
title: "OSX Protip: Show your User's Home Folder in Finder Sidebar"
slug: "/2016/12/show-your-home-folder-in-finder-sidebar/"
tags: [
"osx protips"
]
description: "Learn how to show your User's Home Folder in the Finder Sidebar."
---

If you want to show your Mac's Hard Drive in Finder instead, check out ["Show your Mac's Hard Drive in Finder"](/2014/01/show-your-macs-hard-drive-in-finder-sidebar/).

I spend all of my time in my home folder, and while I don't use Finder very much anymore, I find it useful to have quick access to the home folder. In this quick OSX protip, I’ll guide you through showing the shortcut to your user's Home folder in OSX.

## Show User's Home Folder in Finder Sidebar

![Show User's Home Folder in Finder Sidebar](/img/finder-show-home.gif)

Start by clicking on the Finder icon in your dock, and a new Finder window should come into focus.

In your Finder you should see a sidebar with Favorites and a list of devices. If you don’t see the sidebar, click on “View” in the application menu at the top and then click “Show Sidebar”.

Under Favorites, click the checkbox next to the icon of the home with your username to the right of it. You should now see a link to your user's (in my case, "chaseadamsio") home directory in your Finder sidebar.

As you can see, you also have the ability to show shared items and other external devices. I hide almost all the Favorites (with exception of Applications, Documents &amp; Downloads), show all the devices and turn on all shared but Back to My Mac.

