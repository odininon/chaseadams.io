---
date: "2013-10-01"
title: "Emulate What You Want To Replicate"
slug: "/2013/10/emulate-what-you-want-to-replicate/"
aliases: [
    "/posts/emulate-what-you-want-to-replicate"
]
category: "archive"
---

__Knowing what you want your life to look like is 90% of the battle...the 90% most of us never stop to figure out.__

_"Let's open Chrome's dev tools and set a breakpoint."_ I don't know why that day still stands out to me as one of the most clear memories I have of being at Zappos, but I remember everything about the conversation that followed.

Brian was helping me debug a third party javascript I was having issues with. Watching him step through the file's execution, while also explaining to me exactly what was going on was mind blowing. At that moment I had an epiphany: __I need to soak up everything I can from him.__

I started writing down everything I heard him say in our weekly team gatherings, made mental notes of technologies he mentioned and later researched them, watched him interact with other developers, watched the commits he'd put up in Github.

I started to learn things much quicker and my abilities and skills were sharpened faster because I had found a model of what I wanted that part of my life to look like and emulated it.

This is just one example of one "realm of responsibility" of my life. In my spiritual life, I chose to model my life after men like my college professor, Dr. R, or my Young Life area director, Allen Miller. In my family life my small group leader, Bryson Davis and most obviously my dad. In my problem solving realm, teammates like [Brian Egan](https://www.twitter.com/brianegan), [Nate Weinert](https://www.twitter.com/natebirdman) or Mark Walker. In my creativity, [my mom](https://thecharmhouse.blogspot.com/) has been an incredible inspiration. In all of these realms, these folks have been models I've followed because they __were (and still are) where I want to be.__

We all have something we want to strive towards _being_. Some ideal _person_ that we see in a foggy mirror that through time we hope to see clearly.

The most important thing you can do to grow in your life is to [start by beginning with the end in mind](/2013/10/my-eulogy/), how you want to be remembered, and find the people who most closely model that end. Learn from them. Spend time with them. Watch how they handle their interactions. __Be intentional__ about how you engage with your models. Give them back great amounts of gratitude because they are doing you a great service by figuring out the things you would have to figure out on your own.

__Who do you model your life after? How do you decide who and what areas of life to work on?__
