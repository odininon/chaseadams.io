---
title: "My Story: Why I Believe In the Hour of Code Initiative"
date: "2013-12-08"
slug: "/2013/12/my-story-and-hour-of-code/"
aliases: [
    "/posts/my-story-why-i-believe-in-the-hour-of-code-initiative",
    "/2013/12/08/my-story-and-hour-of-code/",
]
category: "archive"
---


My first job out of college was riding shotgun delivering building supplies. Today I have the honor of working for one of the most renowned enterprise-level retail websites in the U.S. as an Engineer, and it all happened without ever taking a computer science class.

"Can you come home and help us ride this thing out?" I don't remember my dad's exact words, but I do remember a month before I was going to graduate that he asked me if I could come back and help out the family business.

It was the tail-end of 2008 and the economy, as I’m sure you remember, was taking a nose dive into the tanker, with the residential home sector taking a big hit. This was not a good thing for a business whose sole source of income and sales were building materials.

So, with a degree in environmental science in tow, I made the trip back to work with my dad to help out wherever I could: riding shotgun while delivering building materials with some of the brightest and nicest guys I've ever known; filling in for dispatch when the guy who did dispatch had to help pull orders and cleaning toilets. It was hard work.

## A Computer, WordPress, Douglas Crockford &amp; A Little Gumption

Within a few months my dad asked me if I could rebuild our website. I hadn't touched a code editor since I was 16 (in nine years), but that didn't stop me from giving it my best shot. I bought books on HTML &amp; CSS, and rebuilt an informational brochure site with five to six pages, outlining what we did. I was proud of myself. __It catalyzed a feeling I would never forget: I just took a blank directory on a computer and created something out of nothing.__

From this single experience, I decided to buy more books, learn more about programming and understand design principles. Not just aesthetic design, but programming patterns &amp; program arhcitecture. I upgraded our little website to a robust WordPress CMS. __I did it almost all in my own time outside of work or during the trips when I rode shotgun. I had no formal education.__

Three years into my coding journey, my family business stabilized. We found, that through building a better website, more people found us, which led to more inquiries, which led to an increase in sales radius of 20 miles to a coast-to-coast expansion, which led to drop shipping to customers in Arizona, New Jersey and other states. We took our web traffic from 10 visits a day to a peak of 400 visits a day. Learning to code potentially helped save our family business.

Today, through an amazing journey, I have the opportunity to touch code that millions of people see, that contributes to billions in yearly sales and gives customers all across the United States that feeling of "Zappos is a lovely shopping experience, I want to do it again!"

## Take the leap: Learn to Code, Change Your Path

__I want to reiterate this point: I have no formal education in programming.__ This isn't to toot my own horn, because along the way I had the chance to learn from some of the brightest people in the field of Software Engineering both online and in person. I've been "lucky" to have had the experiences I've had, but it all started with me taking initiative. <em>I wish I had a resource like hour of code to catalyze my passion at an earlier age.</em>

__This is the reason I believe everyone in the world with access to a computer should <a href="https://csedweek.org/">do hour of code</a>.__

<img src="https://csedweek.org/images/cs-stats/more-jobs-than-students.png" alt="Computer science is a top paying college degree and computer programming jobs are growing at 2X the national average" />

Source: <a href="https://csedweek.org/promote">Computer Science Education Week: https://csedweek.org/promote</a>

With a computer, you are empowered to potentially fill one of the <em>1.4 million</em> jobs in the United States that goes unfilled because there __just aren't enough people in the world to fill these rolls__ if you just do something.

So how would I suggest you get started? __Learn something.__

Visit the hour of code website and check out one of it's resources.

tldr; Everyone with access to a computer should [do hour of code](https://csedweek.org/), so start your journey!

Three that I recommed:

- __<a href="https://csedweek.org/unplugged/thinkersmith">Thinkersmith's Unplugged Hour of Code Activity</a>__: This one is about learning to solve problems without actually typing a line of code. This is singly the most important aspect of learning to code: Learning to solve problems.
- __<a href="https://www.khanacademy.org/hour-of-code/hour-of-code-tutorial/v/welcome-hour-of-code">Khan Academy</a>__: Khan Academy is easily one of the best websites to learn on, and their hour of code session is learning some basics about Javascript, my favorite language.
- __<a href="https://www.codecademy.com/courses/hour-of-code/0/1">Codecademy</a>__: Codecademy teaches you to write your name in a cool way using Javascript.

So get out there, learn to code. If you don't have a computer, go to the library and try out one of these resources. They don't require an editor, or any special software on the computer. They've removed any barrier to entry for you to have the power to rewrite your future. So get out there:

__<code>console.log('Rewrite your future.');</code>__

<p class="marker">
  A sidenote: If you have kids, at the very least, encourage them to do <a href="https://csedweek.org/learn">hour of code</a>. There's no better opportunity for them than to learn, for free, with great resources, how to write code.
</p>
