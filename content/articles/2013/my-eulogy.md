---
title: "My Eulogy"
date: "2013-10-15"
slug: "/2013/10/my-eulogy/"
aliases: [
    "/posts/my-eulogy"
]
category: "archive"
---

While reading The Seven Habits of Highly Effective People, I was inspired to write my eulogy based on the first habit of "beginning with the end in mind". This is the outcome.

One of John Maxwell's action items in [Seven Habits](https://www.amazon.com/Habits-Highly-Effective-People-Powerful/dp/1451639619/ref=sr_1_1?ie=UTF8&qid=1473713533&sr=8-1&keywords=seven+habits) is to write your eulogy. Seems morbid, right? But what I actually found was that when I started this exercise, I had a clarity of what I wanted the end to look like. It allowed me to sweep away the cruft of the every day to see what was important: the legacy I wanted to leave behind.

__Read this understanding that it is not a picture of who I am, but a template of who I want to be.__ It's not me being boastful (because I'm not this man yet), it's me searching the depths of what I believe to be true and how I want it to shape who I become.

## My Eulogy

<em>The world has lost a great believer, a great husband, a great leader, a great learner, a great teacher, a passionate engineer.

Chase was best known for two qualities: His great joy and wrapping every decision, action, belief, work and person with love.

These two qualities came from an unwavering, deeply anchored sense of who God is. Christ filled his every thought, every action, every interaction so fully that it could be said that his life mirrored Christ as much as any human could. He sought God through a deep thirst for understanding and knowing the scriptures, seeking God's peace and giving God thanksgiving through prayer, and living out both practices through serving others in the ways they most needed to be served: with love and patience.

His great joy was to pursue his wife, Jackie, passionately. To know who she is and to search every nook of her heart, her personality and her mind so well that it could be said that they were of one mind. He understood the true meaning of leadership, that leading meant serving, and no one knew his ability to lead better than she did. His earthly heart was fully hers, and his relationship with her made her both co-dependently and independently stronger as a believer, as a leader, as a daughter and as a sister. He left her with everything she needed to succeed in life.

He believed in people. In their abilities, in their strengths, in their hearts. He did his best to draw those strengths out and amplify them, to help them become stronger in them, and to unpack their weaknesses and learn how to find strength in spite of those areas. He never forgot that in all situations and circumstances that, like himself, at the core of every person was a fallible, broken, deeply-baggaged soul. Knowing that there was an eternal soul at this core, he treated people as humans, with love, gentleness, patience and kindness.

He was a life long learner. He loved to understand as much as he possibly could, sharpening his mind with the tools available to him. It could never be said that he spent a day without trying to fill his mind with something that could be useful. He knew what was important to learn and didn't spend time on learning what was irrelevant or destructive. He believed that one of the strengths of learning was the standing it gave him with people. In being wise and understanding in earthly things he gained a platform to share his understanding of eternal things.

Because of this insatiable desire for learning, he felt a great discontent with himself if he didn't share some bit of knowledge he had that he knew could save someone else from making a poor decision by knowing it too. He understood that the ability to teach was a gift to be cherished. He considered teaching a way to leave everything of who he is to the world. He knew that his body was ephemeral but the gifts he gave others through sharing tempered with love and patience would last for generations.

In his work, through his life, he was a passionate engineer. He loved his work. He found a deep appreciation in building the little things for the complexity and depth of God's creation. He felt that through building things himself, he was giving outward testimony of what he believed about God: That God created all things so simply that to humans they seemed complex. That God created all things to be uniform, not to be chaotic. That God above all created these things as resources for man to flourish.

He did all things wrapped in a deep, unquenchable, unchanging, unexplainable love for God, for his wife and for people.</em>

Writing this was easily one of the most important decisions I ever made for my life. I read it every morning, and though I fall short of it everyday, I grow in some aspect of it. It's a lifelong, slow growth manifesto.

So what about you? Have you written a eulogy or life manifesto? What have you found to be the important things when you peel back the mundane and fillers?
