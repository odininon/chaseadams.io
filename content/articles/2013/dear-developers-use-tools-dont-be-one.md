---
title: "Dear Developer: Use tools, don't be one"
date: "2013-11-07"
slug: "/2013/11/dear-developer-use-tools-dont-be-one-or-proper-etiquite-for-answering-stackoverflow-questions/"
aliases: [
    "/posts/dear-developer-use-tools-dont-be-one"
]
category: "archive"
---

Otherwise known as, better etiquette for answering questions on stackoverflow.com and conversation in general.

I know..._you're awesome_. You know all the latest tricks for writing hip javascript and how to build an compile like a boss. You pretty much __wrote__ the standard for when you should make a method public, private or protected...blah blah blah.

All sass (and not the compilable kind) aside, I'd like to share what I think makes a good developer a _great_ developer: __humility__.

## Geoff Berger: The Most Humble Guy I Know

I used to work with this guy, Geoff Berger. He is one of the smartest guys I know. And not the "I'm a few steps ahead of you" types, but the kind that you think may have been composing music and writing theories of physics when he was waddling around in diapers kind of guys.

The thing is: He'd never admit it to you. __He is the most humble person I've ever met.__

If someone went to him with a question, he'd sit down and somehow in his gentlest way start by asking if you're familiar with some concept about your question, and in a way that made you feel on equal footing with him, not in a belittling way. Then he'd step through and explain how everything was working together, all the time doing it with _passion_ and _excitement_.

Whenever anyone described Geoff the two words that always surfaced were __humble__ and __smartest__. He was always the smartest guy in the room, and he was always humble about it.

__These qualities seem like no brainers, but they're the ones that make for great developers:__

- __Remember other people, just like you, are human.__ If you went to a medical specialist about a health problem, and you weren't an expert, you would want them to treat you like a human, not like some moron off the street who didn't know what squamous cells were and how your late night red bull runs and hackathons are creating them. You'd want them to be kind, and treat you with the respect that you deserve, because you're a human being.
- __Answer the question, and just the question.__ A lot of times, developers, because we're opinionated, like to shove said opinions into a conversation that has nothing to do with it. Keep it as short as it needs to be to answer the question fully. _Note: Brief can be gentle. They're not mutually exclusive._
- __Be excited about it!__ Don't be excited about the fact that someone is listening to you. People will respect (and most likely catch) your excitement for the topic you've got answer to, but don't just talk to have words come out of your mouth.
- __Be humble.__ I can't stress this one enough. I'm not the best at anything. I'm not even close to the best. But I might have answers. Any person's willingness to admit: "I'm not perfect, I'm not always right, and I do make mistakes." is a great springboard for being heard. At the end of the day what I think the answer might be is just _one_ possible solution, and most likely not even the best one for the requirements. I'm not just okay with that, [I'm excited that I get to learn how other people solve problems and grow from it](/2013/10/01/emulate-what-you-wish-to-replicate/).

At the end of the day, we all go home to something, we've all had experiences, we all have baggage. We also all start from somewhere right? I am not the developer I was last year and I'm not the developer I will hopefully be next year. So I want to try, like Geoff did, to treat people that way. To find where they are and work from there. Not treat them as inferiors because they're not where I am.

__The technology field is hot, and it's a great time to be a developer from a "doing great work" standpoint, but it doesn't release us from the responsibilities of remembering that our peers are human beings. They exist after the experiences they have with us, and those experiences _shape_ their journey as much as it shapes ours.__

__P.S. If you read this entire post and at this point felt like you were being belittled, take that feeling and tuck it away for next time you're on my side of the discussion.__
