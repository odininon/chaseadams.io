---
title: "3 Resources For Code Sharing & Collaboration"
date: "2013-10-07"
slug: "/2013/10/3-resources-for-sharing-code-snippets/"
tags: [
    "web development",
]
aliases: [
    "/2013/10/3-resources-for-code-sharing-collaboration/",
    "/posts/3-resources-for-code-sharing-collaboration"
]
---

If you're tired of sharing code snippets for other people to look at or collaborate on via [github.com](https://www.github.com), [gist.com](https://www.gist.github.com) or worse, dropbox or email, check out these three great code collaboration tools.


Collaborating on code is one of the most vital tools a developer can leverage.

We solve problems best by having a fresh set of eyes join in and talking through our solutions. Our code matures faster through having stronger developers give input. We learn more clearly by having tangible examples both in the code itself and in it's execution.

If you are like me, at one point in time the best option you thought you had was to email it. Eventually you probably learned about github.com and gist, where you could share code people could pull down locally. Unfortunately, all of these solutions are expensive to the person on the other side in both time and work.

Now frontend development is getting major contributions from both the community and companies like Google and Mozilla to help speed up our workflow. From the community, we have 3 great tools for sharing code snippets that show the code and it's execution:

- __Codepen.io__ for mocking presentational ideas with HTML &amp; CSS
- __JSFiddle.net__ for javascript interaction with the presentation layer
- __JSBin.com__ for javascript interaction and the data/event layer

<!-- more -->

## codepen.io

[codepen.io (link)](https://www.codepen.io)

![Codepen.io](https://www.realchaseadams.com/imgs/2014/01/codepenio-1024x633.png) [Codepen Example](https://codepen.io/realchaseadams/pen/wLcse)

<h4>What I use it for: Mocking Presentational Ideas with HTML &amp; CSS</h4>

Codepen.io is yet another great idea from [Chris Coyier](https://chriscoyier.net/). Three of the main benefits of using it that really stand out to me are:

- It's easy to browse other people's pens for inspiration
- Forking other people's pens
- The options for HTML, CSS &amp; JS are incredibly robust

I generally use it when I've got __HTML__ and/or __CSS__ ideas I want to quickly share and I think someone else that I want to collaborate with will want to edit it in their own fork to share back with me.

The language options are awesome. For HTML, you get the option of using HTML, markdown, slim or jade and adding classes to the html or adding meta data to the head. For CSS, you get sass (both .sass and .scss), less and stylus. __These features alone, in my opinion, make this code collaboration tool king for presentational concepts.__

## jsfiddle.net

[jsfiddle.net (link)](https://www.jsfiddle.net)

![jsfiddle.net](https://www.realchaseadams.com/imgs/2014/01/jsfiddle-1024x633.png) [JSFiddle Example](https://jsfiddle.net/rfwEP/)

<h4>What I use it for: Javascript Interaction with the Presentation Layer</h4>

At the office when we're talking about abstract concepts in javascript, fiddles get passed around a lot. We used JSFiddle for training our intern and it was a great way for him to do work and show us his progress.

You can save versions of your fiddle to share, which was great for us to give him a problem set and then have solutions in a different version for him to go to when he completed his solutions.

It also has a collaboration feature (which is in beta), so if you want to have multiple people in a fiddle working on it, it's the ideal feature!

## jsbin.com

[jsbin.com (link)](https://www.jsbin.com)

![jsbin.com](https://www.realchaseadams.com/imgs/2014/01/jsbin-1024x633.png) [JSBin Example](https://jsbin.com/UWuwiyi/1/edit)

![jsbin.com](https://www.realchaseadams.com/imgs/2014/01/jsbin2-1024x633.png) [JSBin Date Example](https://jsbin.com/UWuwiyi/2/edit)

<h4>What I use it for: Javascript that focuses on data</h4>

Sometimes you need to be able to see what's under the hood when you're working with javascript. JSBin allows you to have a console (that's not your browser console), that can be run and viewed easily.

If you're working on any javascript that isn't modifying the interaction layer, you need to make sure that it's functioning as you expect and JSBin is a great tool for doing that.

__Do you know of any good sources for collaboration that other developers should know about? If so, share them!__
