---
title: "Making Work Visible [Book Review]"
description: "Making Work Visible is a canonical book for anyone who wants to be effective at getting the right things done in the right amount of time."
date: "2018-01-28"
slug: "/book-review-making-work-visible/"
tags: ["kanban"]
---

I discovered *[Making Work Visible](https://www.walmart.com/ip/Making-Work-Visible/56040750)* when I needed it most and was most ready for it. It is a canonical book for anyone who wants to be effective at getting the right things done in the right amount of time.

# Why It Mattered To Me

<img src="https://images-na.ssl-images-amazon.com/images/I/41jfDk7D4GL._SX334_BO1,204,203,200_.jpg" height=200 class="fl pr3" />

I was two months into my journey as a manager for a small technical team whose primary responsibility was to manage an overwhelmingly active queue of support requests. 

I was struggling with two major tensions: 

1. "How do I know the work we're doing is the right work for the right time?"
1. "How do I ensure we're able to be effective?"

I wanted to increase our internal customers' velocity and happiness by decreasing the request resolution time without increasing risk (production systems) or hours at work. 

I was uncertain (and lacked confidence) of how to best visualize the team's work to uncover our constraints (what [*Making Work Visible*](https://www.walmart.com/ip/Making-Work-Visible/56040750) calls **Time Thieves**). 

*[Making Work Visible](https://www.walmart.com/ip/Making-Work-Visible/56040750)* equipped me with tools to ask the right questions to find clarity about my tensions and overcome my lack of confidence. In the short amount of time between finishing the book and now, my team has made a number of small changes to help us expose the 5 time thieves. In the past two weeks, I've received feedback that these changes have made our internal customers feel better supported and engaged, given back our sibling teams time to spend on planned work without increasing headcount or the amount of hours my team is working (my next goal is to reduce the amount of hours we spend on this work).

# Key Takeaways

Setup a workflow system to do five things:

> The solution is to design and use a workflow system that does the following five things: Make work visible. Limit work-in-progress (WIP). Measure and manage the flow of work. Prioritize effectively (this one may be a challenge, but stay with me—I’ll show you how). Make adjustments based on learnings from feedback and metrics.

## Exposing The 5 Time Thieves

*[Making Work Visible](https://www.walmart.com/ip/Making-Work-Visible/56040750)* has a number of exercises to try out with teams to expose the time thieves. The ones that resonated with me to start with were "Explore the Five Reasons Why We Take on More WIP" & "Demand Analysis". 

## How Is Work Prioritized

There's the way you *want* work to be prioritized and the way it *is* prioritized. Creating a label for who asks for a task, tracking whether it's planned or unplanned, and how compelled we are to get it done will enable us to determine how the work is *truly* prioritized. If we can be aware of how work is expected to be prioritized, we can have a deeper discussion with the person who asks for that work to help us increase visibility into the things they're going to ask for with more notice.

## Lean Coffee

"Lean Coffee"—a meeting format created by Jim Benson:

> Lean Coffee turns traditional, one-direction management meetings on its head by helping teams uncover the most important topics to the majority of people, by allowing everyone to hear and to be heard, and by providing real-time feedback.

I've always disliked agenda-less meetings, but the idea of a meeting that's purpose is to determine what the important topics are and talk about those topics? That's a meeting format I can get behind. I'm excited to try this style of meeting out with my team (as well as a few non-work meetings I'm a part of) to uncover what's really important and needs to be talked about and talking about those topics.

## Other Topics I Learned About:

- 5 Time Thieves
- Kanban
- Flow time
- Operations Reviews
- Queueing Theory

## Some Favorite Highlights

> We allow the chaos of modern work coupled with an often paralyzing number of options at our disposal to overload us, to distract us, to stealthily steal our time and focus and ultimately impede our effectiveness.

> All it takes is a shift from haphazardly saying yes to everything to deliberately saying yes to only the most important thing at that time. And to do it visually.

> Indeed, time is sacred. Treat it as such. Visualize your work. Limit the amount of work you take on. Pay attention to its flow. Build thoughtful work systems to reflect what really matters. To breathe. To think. To learn. To grow. To play. To love. To live. For it is in working well that we can live well.

## Added Bonuses

**Book recommendations abound.** There were at least 4 other books that Dominica mentioned that caught my attention and are now on my list of books to read.

**Quotes.** Every chapter starts with a quote and I found myself highlighting almost every one.

# Who Should Read It?

If someone has asked you "how are you doing?" and you responded with, "I'm (SO,TOO,VERY) busy!" or "I'm working way too much.", read this book.

If you are on a team where no one knows what anyone else is doing, read this book.

If you know you could be *completing* (operative word) more work in less time but can't determine why you're unable to do it, read this book. 

If you find yourself with a million things to do and you suffer from deciding where to start, read this book.

This book doesn't encourage overwork or hero mentality, rather it gives you the tools to ask, "how can I be more effective without spending an insane amount of hours working?"

**If you enjoyed this review,  please consider buying *Making Work Visible* from [Walmart](https://www.walmart.com/ip/Making-Work-Visible/56040750) (you'll be supporting me in an indirect way). If you support your local library, be sure to check for it in eBook or audiobook through [Libby by OverDrive](https://meet.libbyapp.com/). If you'd prefer to buy it on Amazon, you can buy it in [paperback](https://www.amazon.com/Making-Work-Visible-Exposing-Optimize/dp/1942788150/ref=tmm_pap_swatch_0?_encoding=UTF8&qid=1517151393&sr=8-1), [kindle](https://www.amazon.com/Making-Work-Visible-Exposing-Optimize-ebook/dp/B076BYZ6VN/ref=sr_1_1?ie=UTF8&qid=1517151393&sr=8-1&keywords=making+work+visible) or [audiobook](https://www.amazon.com/Making-Work-Visible-Exposing-Optimize/dp/B07776XY3D/ref=tmm_aud_swatch_0?_encoding=UTF8&qid=1517151393&sr=8-1).**