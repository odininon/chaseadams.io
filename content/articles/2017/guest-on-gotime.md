---
title: "Guest on GoTime: Go at Walmart (and Scale)"
slug: "/guest-on-gotime-go-at-walmart-and-scale/"
date: "2017-08-18"
description: "I joined GoTime to talk about working on distributed systems with distributed teams, giving people opportunities to learn and grow, and interesting Go projects and news."
---

I joined the show to talk about working on distributed systems with distributed teams, giving people opportunities to learn and grow, and interesting Go projects and news.

My Intro:

They call me chaseadamsio, <br />
I love building software you know, <br />
and I'm happy to say, that there's no better way, <br />
than to power said software with go.

<audio data-theme="night" data-src="https://changelog.com/gotime/54/embed" src="https://cdn.changelog.com/uploads/gotime/54/go-time-54.mp3" preload="none" class="changelog-episode" controls></audio><p><a href="https://changelog.com/gotime/54">Go Time 54: Go at Walmart (and Scale) with Chase Adams</a> – Listen on <a href="https://changelog.com/">Changelog.com</a></p><script async src="//cdn.changelog.com/embed.js"></script>
