---
date: "2017-01-01T04:00:00Z"
title: "My 3 Themes for 2017"
slug: "/2017/3-themes/"
description: "The 3 themes I chose for 2017: My Pack, Kaizen, Miracle Grow."
category: "archive"
---

Last year I started an exercise that changed how I approached my year, quarter, month, week and day: Theming. 

The idea is that rather than creating New Year's resolutions, I choose themes that best represent what I want to focus on for the year at a high level using words or phrases that inpsire me to pursue the best version of myself.

Choosing themes for the year gives me freedom to choose what I think will bring value to my life. An abstract theme allows me to focus on what's important while also guiding me to choose tasks that have more clear action as I drill down to smaller chunks of time.

Although I call it theming and pursue it with a more systematic approach, the idea is heavily based on [Chris Brogan's "Three Words" concept](https://chrisbrogan.com/3words2017/).

## Previous Themes

- [2016: Systematic, Rigour, Discovery](/2016/3-words)

## How I'll Practice It

This year, I want to iterate on my process for how I practice my 3 themes (changing the name of "my 3 words" from last year to "my 3 themes" is a good example of a small change). 

A part of iteration is reflection, so I'll be doing the following retrospectives:

- Daily. A simple yes/no to "Was I successful at moving forward in my 3 themes?" and a single sentence answerinf "How did it make me feel to move forward (or not move forward)?"
- Weekly. Reflecting on the daily retrospectives and answering "did each day aggregate into a consistent change towards my intentionally chosen monthly theme?" 
- Monthly. Reflecting on the weekly retrospectives and answering "what was the biggest impact I had in each theme?"
- Quarterly. Reflecting on my big picture: "How have I changed as a result of practicing and growing in these three themes?"
- Yearly. Asking the big question: "How did I change the world?"

## My 3 Themes

My 3 Themes for 2017 are:

- The Pack
- Kaizen
- Miracle Grow

### The Pack

_As in **my** pack; my nuclear family unit: Jackie, Elle, Tag & Belle._

<img src="/img/chase-and-elle.jpg" />

In 2016, Elizabeth (I call her Elle) joined our family and stole our hearts. 2017 will be the first full year of having her in our lives and I want my presence in her life to be special and impactful.

Having Elizabeth in the family also means that Jackie spends a lot of time focusing on helping her grow (I've felt a new dimension of love for Jackie watching her be a mom) both physically and cognitively which means Jackie's focusing on Elle's needs, not her own. **I want to be a great servant husband, a good playful dad and everything else is triaged after that.**

With a new baby in the house, the dogs are realizing they're not the most important dependents in the house anymore, so I'm also going to spend a lot of time making sure they feel loved.

#### How will I know if I was successful?

I should be able to answer yes to the following questions:

- Did Jackie feel like we were equal (as equal as a dad of a breast fed baby can be) partners in raising Elle in the four realms of life (spiritual, physical, intellectual, emotional)?
- Did Jackie feel pampered? Did she feel like I did everything in my power to make sure she had space to recharge the batteries in the four realms of her life? 
- Did Jackie feel loved more than I loved her last year?
- Did Elle experience the deep love I have for her as a dad?
- Did I actively choose to engage in Elle's day to help her grow stronger in her four realms?
- Did the dogs get enough belly scratches, treats, walks and playtime to feel like they are still part of our pack?

### Kaizen

_Small changes for the better._

Kaizen is a practice of choosing small (sometimes ridiculously small) changes to make in your life for the better. I was first introduced to Kaizen a few years ago but it didn't take seed in my brain until my previous manager (and current co-mentor) Dave Cadwallader brought it up in conversation a few times in 2016 (He wrote about [Kaizen here](https://geekdave.com/2013/08/03/what-developers-can-learn-from-a-harlem-soup-kitchen/)).

I read Robert Maurer's [One Small Step Can Change Your Life: The Kaizen Way](https://www.amazon.com/Small-Step-Change-Your-Life/dp/076118032X) last week and one of the most important takeaways for me was this quote that I've taken to be a daily affirmation: "The journey of a thousand miles starts with one small step". 

One of the recommendations Maurer made in "One Small Step Can Change Your Life" was to pick something so ridiculously small that you can't help but do it. He pointed out that our brains are naturally wired to go into fight or flight mode when something is too complicated. Anyone who's made New Years Resolutions knows he's right about that!

#### How will I know if I was successful?

I should be able to answer yes to the following questions:

- Did I pick a small actionable task that helps me move towards my larger goals?
- Did I pick the right small actionable tasks?
- Did I stick to a habit I would've otherwise neglected because of the small actionable tasks I chose?
- Was I able to overcome my "lizard brain" (or better yet, totally bypass it) with the small tasks I chose?
- Did my small changes make me better overall?

### Miracle Grow

_As in the stuff that put on plants to make them grow...miraculously._

I've been putting together a list of people who I think I have the ability to use my talents to accelerate their growth and I want to help those people succeed in 2017 (and beyond!).

It doesn't mean that I won't help people outside of that list (part of Kaizen is change) but it does mean that I want to be intentional about making sure the effort I make contributes to the growth of people I know can benefit from the strengths I bring to the table.

There's also a minor personal aspect to this theme: "what is my Miracle Grow?" What 20% of actions contribute to 80% of my growth (oh hey, did I just cross-pollenate Miracle Grow with Kaizen?!)?

#### How will I know if I was successful?

I should be able to answer yes to the following questions:

- Did I invest in the life of someone else?
- Did I use my strengths to amplify the strengths of others?
- Did I help raise the voice of the voiceless?
- Did I use my strengths to help those who are too timid to use their voice to find confidence in their voice?
- Did I make a difference in the world by contributing the best parts of myself to the lives of those around me?
- Did I share what I'm learning with others in such a way that I helped them grow? 
- Was I able to share my strengths, gifts, knowledge an experience in a way that allowed me to focus on the people I was intentional about helping?

**With that, welcome to 2017! I hope you find your desired output, determine your intentional inputs and take action in 2017 in ways that help you glow in the dark!**
