---
title: "Migrating My Static Site From Hugo to GatsbyJS"
date: "2018-07-01"
description: "A guide on why and how I migrated my static website from Hugo to GatsbyJS@2.x.x"
---

# Why I Migrated From Hugo to GatsbyJS

- not for build times!!!
- wanted to write react
- wanted server/client side rendering
- like the idea of expanding with plugins rather than contributing to the core

# How I Migrated From Hugo to GatsbyJS

- install the gastby CLI globally (my least favorite part)
- 