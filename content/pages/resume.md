---
title: Resume
slug: "/resume/"
date: 2014-06-21T02:47:00Z
---

# Chase Adams
*Building A Better Web*

- Las Vegas, NV 89183
- Email: [realchaseadams@gmail.com](mailto:realchaseadams@gmail.com)
- Site: [chaseadams.io](https://chaseadams.io)

## Experience

### Engineering Manager, Edge Consulting @ [Walmart Labs](https://www.walmartlabs.com/)

*November 2017 - Present*

- Coordinated multiple projects between business needs and market operations 
- Helped onboard new acquisitions and sites to Edge Proxy/Multi-CDN product
- Led training on leveraging cache and ESI for performance
- Worked with major tenants on Lua plugins for edge proxy
- Led team in capturing work and bucketing common work
- Coached team members on debugging and troubleshooting HTTP traffic flows

### Senior Software Engineer, Edge Platform @ [Walmart Labs](https://www.walmartlabs.com/)

*January 2017 - November 2017*

- Setup Elastic Search/Kibana cluster that a NSQ daemon in place of logstash to collect 25x more longs than with our original ELK stack
- Helped provide better visibility into odnd/customer issues (ENK stack) with better throughput of logs and longer storage 
- Worked with internal customers to better understand how to use tools (curl, admind) and applied fixes to customer reported bugs (HTML encoded match URLs, last known good response) as well as making tools more robust (verified assets reporting for tenant debugging)
- Migrated core products to deployment infrastructure (OneOps)
- Reduced burden of operations with salt
- Led Lua scripting 101 training for tenants and partners 
- Worked with tenants to audit Lua plugins for edge proxy for performance 
- Fixed a bug causing issues with EXIF orientation 
- Increaased visibility into software operations 

### Lead Software Engineer, CI/CD for Customer Experience Engineering @ [Walmart Labs](https://www.walmartlabs.com/)

*January 2016 - January 2017*

- Led CI/CD initiative for all react component libraries and 12 track teams 
- Created pipelines that facilitated 2 week release cycles becoming daily deployments
- Trained CI/CD team members on debugging build logs for track teams 
- Created and streamlined Pull Request Verification process for better and faster feedback
- Created processes and documentation to alleviate CI/CD team from needing to answer common build issues

### Senior Software Engineer, App Platform @ [Walmart Labs](https://www.walmartlabs.com/)

*July 2015 - December 2015*

- Created tooling to maintain dependencies 60+ react component library repositories 
- Integrated checks for lint and unit tests for 60+ (and growing) react component libraries
- Collaborated on creating best practices and core components for track teams for walmart.com
- Educated individuals and teams on debugging practices and internal best practices

### Frontend Instructor @ [The Iron Yard](https://www.theironyard.com)

*July 2015 - October 2015*

- Taught 5 new frontend developers to build the web using the following technologies: HTML, CSS, Javascript & Git

### Mobile Web Lead @ [Zappos.com](https://www.zappos.com)

*February 2012 - July 2015*

- Built multiple micro-applications for internal teams using HapiJS and hand-rolled GoLang server framework.
- Contributed to architecture of mdot Zappos Family of Sites build tool.
- Contributed to m.zappos.com, m.couture.zappos.com and m.6pm.com.
- Implemented lean a/b tests on desktop Login page.
- Contributed to Frontend ruby packing script for sass, css & javascript.
- Built redesigned www.6pm.com product page.
- Minor contributions in backend Spring Java application.
- Shepherded multiple full stack releases.
- Co-ordinated 6 concurrent stacks releasing a global change.
- Mentored interns & junior developers in problem solving, sass, freemarker and javascript.
- Taught courses for our internal TechUniversity.
- Contributed to Frontend Team culture & technical growth.

### Web Developer @ [Rock, Paper, Scissors](https://www.123shoot.com)

*February 2011 - February 2012*

- Built & developed framework for creating WordPress sites for clients faster.
- Built & maintained over 15 wordpress sites.
- Maintained Zend Framework as CMS sites.
- Started journey into Ruby on Rails & Ruby Deploy Tools.

### Web Developer @ [Cofer/Adams Building Center](https://www.coferadams.com)

*December 2008 - February 2011*

- Created CMS website using WordPress.
- Increased traffic from 20-30 visits per day to 250-300 visits per day over the course 8 months.
- Managed search engine campaigns, bumped coferadams.com to top 3 spot on Google for over 300 relevant long-tail keywords.

## Education

### B.S., Environmental Science @ Liberty University, Lynchburg, VA

*Spring 2004 - Fall 2008*

- Participated in salamander, owl and turtle research in the field & in the lab.
- Took multiple business courses including Accounting, Macroeconomics, Microeconomics & Business Marketing
- Organized Young Life Events for high schoolers.

### General Studies @ University of Georgia, Athens, GA

*August 2002 - Spring 2004*

- Took my first computer science course.

## Skills

### Languages

- XHTML1, HTML5
- CSS2, CSS3
- JavaScript (*Client-Side & Server-Side (NodeJS)*)
- Ruby
- GoLang
- Python
- Bash

### Libraries & Build Tools

- jQuery, Backbone.js & Underscore.js
- [Yeoman](https://www.yeoman.io), [Grunt.js](https://www.gruntjs.com), [Bower.js](https://www.bower.io)
- [Assemble](https://github.com/assemble/assemble) & [Hugo](hugo.spf13.com)
- Freemarker (Java) & Handlebars (Ruby & Javascript)

### Software

- Git
- SublimeText 2
- Terminal & iTerm2
- Vim
- JSFiddle.net
- CodePen.io
- SVN

### Soft Skills

- Mentoring
- Teaching
- Fine-tuning team & company culture
- Minor Data Interpretation

## Interests

*climbing, bouldering, hiking, dog park, reading, stackoverflow.com, music, learning new languages, teaching, kayaking*

