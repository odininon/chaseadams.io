---
title: What Are My Strengths?
slug: strengths
date: 2016-11-11T17:27:29Z
---

I took the StrengthsFinder 2.0 assessment, and the results were:

- Learner
- Connectedness
- Responsibility
- Intellection
- Achiever

## Learner

- I have an insatiable desire to know as much as I can about a lot of topics
- I really like to hear people share something they're excited about and dig into that topic on my own later
- The process of learning fascinates me...how our brains are able to retain not only information, but how that information might apply or connect to some other bit of information

### Things I Try To Watch For

It's really easy for me to always "chase" the next best things, especially in technology. Because I'm a learner, I don't just want to know about my core compentency, I want to know about all of the technologies I can. I try to use my strength of **responsibility** to know when it's prudent to chase a topic, if it's prudent and if it's worthwhile sharing with other people.

Because I'm an **achiever**, it's really easy for my thirst to learn to stoke the fire of feeling accomplished, but the degree at which I thirst for learning sometimes leaves me feeling like, although I may have had a _really_ productive day, I wasn't productive.

## Connectedness

- People are important to me. Not just as people, but that they feel like they belong
- I believe there is a purpose for everyone's existence and that it is important...and something bigger than any of us as individuals or even as groups
- I like to find the nodes where people, places and things connect. Finding those nodes is kind of like finding an unknown treasure after a long search.

### Things I Try To Watch For

When you have a strength like this, it's really easy to come across as insincere because it comes with a lot more excitement and vigour than any of the other strengths, especially combined with the fact that most of my other strengths are very easily practiced in solitude or isolation. That could leave some people (especially people I meet for the first time) feeling one of two ways: really annoyed at the excitement or feeling that I'm insincere and have alterior motives.

There's really no way to change that about myself, especially initially, but I believe with time those who might be initially annoyed or agitated grow to at least tolerate it (and I'll usually dial it back if I notice it) and the genuineness comes through.

## Responsibility

- I feel emotionally bound to the things I commit to
- It's a great feeling to know that people think I'm dependable or that something I've done makes their responsibilities easier
- I always want to do my best to fully come through to meet expectations and I claim full ownership psychologically

### Things I Try To Watch For

The **achiever** in me is always saying, "you can take on that extra thing" and the **responsibility** only speaks up after I've made a commitment. These two strengths are probably the most harmful because I allow the **achiever** to trump the **responsibility**. The really awful part about that is that the **achiever** ends up feeling discouraged because I'm not able to get everything done and the responsibility in me is discouraged because I feel like I committed to too many things and that impacts my self-worth very deeply.

## Intellection

- The ability to reflect on the day is incredibly important to me and helps me grow and modify my actions so that I can achieve more and satisfy my responsibilities.
- Thinking about processes that make up my life and the systems around me allows me to make the connections (whether immediately or later through processing by my subconcious) I need to make

### Things I Try To Watch For

This **intellection** can cause me to lose focus sometimes (I've had many times when I've missed an exit while driving because of this mental exercising). The biggest issue is that it can cause me to over-assess a situation or a process, which can be harmful to my psyche if the situation has been resolved and to the process if it's at a good state.

## Achiever

- I like to get things done
- I want my days to feel full of impact and to feel effective
- I am willing to put in extra time on a project to get it right, to get up early and sleep less to get more done
- I get a great deal of satisfaction in seeing the "needle move"

### Things I Try To Watch For

At this point, I think I've pretty much beat up the achiever in me pretty hard. The most important thing is that I try not to let the **achiever** cause me to be discouraged, but rather, use that desire to achieve more to stoke the fire so I don't burn out.

