---
title: "What I'm Doing Now"
slug: "/now/"
date: "2016-02-02T21:00:00"
updated: "2017-01-06T21:00:00"
---

I live in Las Vegas, Nevada, daily discovering what it means to live an essential life. The broad strokes of what I'm doing this year (2017) are...

1. enjoying every microsecond of being present with my "pack"
1. growing through tiny changes
1. using my skills and talents to help people around me accelerate their growth

You can read more about it by checking out [my three themes for 2017](/2017/3-themes). 

In my day-to-day, I'm focusing on...

- Being present with Jackie (my wife), Elle (my bugaboo), Tag & Belle (our two pups)
- Basking in this glorious season of being a new dad
- Maintaining/contributing to the software that makes the Walmart request/response lifecycle faster and more perfomant using the Go language
- Learning to use Emacs to be a more effective software engineer
- Reading books 
- Writing about distributed teams, productivity and general software practices 
- Investing time and energy in helping those around me attain an essential life
 
I'm incredibly happy where I am in life: watching my little girl experience the newness of life, loving on my wife, learning a ton and building high throughput distributed software for fun (and getting paid to do it!). 

I try to keep this page updated regularly, so be sure to keep checking back if you're curious what I'm up to!
