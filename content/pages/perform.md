---
title: "How Do I Perform?"
date: 2016-11-11T17:29:07Z
slug: perform
---

## How Do I Learn?

I have a multi-modal learning preference, which means I can learn in all modes, but it's possible that using more than one mode solidifies concepts. I believe I learn best by engaging in mutiple modes.

### Kinesthetic (Tied for Strongest: 13)

Description: This preference uses your experiences and the things that are real even when they are shown in pictures and on screens.

Key words: senses, practical exercises, examples, cases, trial and error

[Kinesthetic Strategies](https://vark-learn.com/strategies/kinesthetic-strategies/)

### Aural (Tied for Strongest: 13)

Key words: listening, discussing, talking, questioning, recalling

Description: This preference is for information that is spoken or heard and the use of questioning is an important part of a learning strategy for those with this preference.

[Aural Strategies](https://vark-learn.com/strategies/aural-strategies/)

### Visual (Strong: 10)

Key words: different formats, space, graphs, charts, diagrams, maps and plans

Description: This preference uses symbolism and different formats, fonts and colors to emphasise important points. It does not include video and pictures that show real images and it is not Visual merely because it is shown on a screen.

[Visual Strategies](https://vark-learn.com/strategies/visual-strategies/)

### Read/Write (Moderately Strong: 9)

Key words: lists, notes and text in all its formats and whether in print or online

Description: This preference uses the printed word as the most important way to convey and receive information.

[Read/Write Strategies](https://vark-learn.com/strategies/readwrite-strategies/)
